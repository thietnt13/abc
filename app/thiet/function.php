<?php
function dsCategory($data, $parent = 0, $str = "", $select = 0)
{
    foreach ($data as $key => $val) {
        $id = $val->id;
        $name = $val->name;
        if ($val->parent_cat == $parent) {
            if ($select !== 0 && $id == $select) {
                echo "<option value='$id' selected='selected'>$str $name</option>";
            } else {
                echo "<option value='$id'>$str $name</option>";
            }
            dsCategory($data, $id, $str . '|--', $select);
        }
    }
}

function checkUserCategory($data, $categorySelect, $categoryUser)
{
    if (array_key_exists($categorySelect, $categoryUser)) {
        return true;
    } else {
        foreach ($data as $key) {
            if ($categorySelect == $key->id)
                return checkUserCategory($data, $key->parent_cat, $categoryUser);
        }
    }
}

function listCategoryByUser($data, $parent = 0, $str = "", $categoryUser, $select = 0)
{
    foreach ($data as $key => $val) {
        $id = $val->id;
        $name = $val->name;
        if ($val->parent_cat == $parent) {
            if (checkUserCategory($data, $val->id, $categoryUser)) {
                if ($select !== 0 && $id == $select) {
                    echo "<option value='$id' selected='selected'>$str $name</option>";
                } else {
                    echo "<option value='$id'>$str $name</option>";
                }
            }
            listCategoryByUser($data, $id, $str . '|--', $categoryUser, $select);
        }
    }
}

function arrayCategoryUser($data, $categoryUser)
{
    $arrayCategory = array();
    foreach ($data as $item) {

        if (checkUserCategory($data, $item->id, $categoryUser)) {
            array_push($arrayCategory, $item->id);
        }
    }
    return $arrayCategory;
}


?>
