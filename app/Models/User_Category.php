<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\User;

class User_Category extends Model
{
    protected $table='User_Category';
    protected $fillable = ['user_id', 'category_id'];

    public function Category(){
        return $this->hasOne('Category','id_category','id');
    }

    public function User(){
        return $this->hasOne('User','id_user','id');
    }
}
