<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Post extends Model
{
    protected $table='Post';
    protected $fillable = ['name', 'category_id','title','content','thumbnail','image','status'];
    public function Category(){
        return $this->hasOne('Category','category_id','id');
    }
}
