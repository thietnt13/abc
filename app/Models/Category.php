<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
use App\Models\User_Category;

class Category extends Model
{
    protected $table='category';
    protected $fillable = ['name', 'parent_cat'];
    public function Post()
    {
        return $this->belongsTo('Post','category_id','id');
    }

    public function User_Category(){
        return $this->belongsTo('User_Category','id_category','id');
    }
}
