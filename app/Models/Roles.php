<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Roles_Permission;

class Roles extends Model
{
    protected $table='Roles';
    protected $fillable = ['name'];
    public function User(){
        return $this->belongsTo('User','roles_id','id');
    }

    public function Roles_Permission(){
        return $this->belongsTo('Roles_Permission','id_roles','id');
    }
}
