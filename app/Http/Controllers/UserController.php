<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\User_Category;
use Illuminate\Http\Request;
use App\User;
use App\Models\Roles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use function PHPSTORM_META\elementType;

class UserController extends Controller
{
    public function getList()
    {
        $lstUser = DB::table('users')
            ->join('roles', 'users.roles_id', '=', 'roles.id')
            ->select('users.*', 'roles.name as roles')
            ->paginate(10);
        return view('admin.user.list', ['user' => $lstUser]);
    }

    public function getThem()
    {
        $roles = Roles::all();
        $user = Auth::user();
        $listCategory = DB::table('category')->where('del_flg','=',0)->get();
        if ($user->can('create', User::class)) {
            $categoryUser = DB::table('user_category')->select('user_category.category_id')->where(['user_id'=>$user->id],['del_flg'=>0])->get()->keyBy('category_id')->toArray();
            return view('admin.user.them', ['roles' => $roles, 'category' => $listCategory, 'user' => $user, 'categoryUser' => $categoryUser]);
        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function postThem(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required|unique:users|max:125|min:12',
                'name' => 'required',
                'roles' => 'required',
                'password' => 'required|min:4|max:12',
                'category' => 'required'
            ],
            [
                'email.required' => 'Email không được để trống',
                'email.max' => 'Email này đã vượt quá 125 kí tự.',
                'password.max' => 'Password này đã vượt quá 10 kí tự.',
                'email.min' => 'Email phải lớn hơn 12 kí tự.',
                'password.min' => 'Password phải lớn hơn 4 kí tự.',
                'email.unique' => 'Email này đã tồn tại.',
                'name.required' => 'Tên không được để trống',
                'password.required' => 'Password không được để trống',
                'category.required' => 'category không được để trống',
                'roles.required' => 'category không được để trống',
            ]);
        $user = Auth::user();
        if ($user->can('create', User::class)) {
            if ($user->roles_id == 2 && $request->roles != 3) {
                $notification = array(
                    'messege' => 'Không có quyền',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification)->withInput();
            } else {
                $listCategorySelect = $request->input('category');
                $listCategory = DB::table('category')->get();
                $categoryUser = DB::table('user_category')->select('user_category.category_id')->where(['user_id'=> $user->id],['del_flg'=>0])->get()->keyBy('category_id')->toArray();

                foreach ($listCategorySelect as $item) {
                    if (checkUserCategory($listCategory, $item, $categoryUser)) {
                        DB::beginTransaction();
                        try {
                            if ($request->password == $request->repassword) {
                                $newUser = User::create([
                                    'name' => $request->name,
                                    'email' => $request->email,
                                    'roles_id' => $request->roles,
                                    'password' => bcrypt($request->password)
                                ]);
                                $userId = $newUser->id;
                                $dataInsert = [];
                                foreach ($listCategorySelect as $item) {
                                    $data = [
                                        'user_id' => $userId,
                                        'category_id' => $item
                                    ];
                                    $dataInsert[] = $data;
                                }
                                User_Category::insert($dataInsert);
                            } else {
                                $notification = array(
                                    'messege' => 'Mật khẩu chưa khớp',
                                    'alert-type' => 'error'
                                );
                                return redirect()->back()->with($notification)->withInput();
                            }

                        } catch (ValidationException $e) {
                            DB::rollBack();
                            return Redirect()->back()
                                ->withErrors($e->getErrors())
                                ->withInput();
                        } catch (\Exception $e) {
                            DB::rollBack();
                            throw $e;
                        }
                        DB::commit();
                        $notification = array(
                            'messege' => 'Thêm user thành công',
                            'alert-type' => 'success'
                        );
                        return redirect()->route('user.list')->with($notification);
                    } else {
                        $notification = array(
                            'messege' => 'Không có quyền',
                            'alert-type' => 'error'
                        );
                        return redirect()->back()->with($notification)->withInput();
                    }
                }


            }
        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification)->withInput();
        }
    }

    public function getSua($id)
    {
        $user = DB::table('users')->where([['del_flg','=',0],['id','=',$id]])->first();
        $roles = Roles::all();
        $listCategory = Category::all();
        $categoryByUser = DB::table('user_category')->select('user_category.category_id')->where(['user_id'=>$id],['del_flg'=>0])->get()->keyBy('category_id')->toArray();
        if ($this->updateUser($user, $id)) {
            return view('admin.user.sua', ['user' => $user, 'roles' => $roles, 'listCategory' => $listCategory, 'categoryByUser' => $categoryByUser]);
        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function postSua(Request $request, $id)
    {
        $this->validate($request,
            [
                'email' => 'required|unique:users|max:125|min:12',
                'ten' => 'required',
                'password' => 'required|min:4|max:10'
            ],
            [
                'email.required' => 'Email không được để trống',
                'email.max' => 'Email này đã vượt quá 125 kí tự.',
                'password.max' => 'Password này đã vượt quá 10 kí tự.',
                'email.min' => 'Email phải lớn hơn 12 kí tự.',
                'password.min' => 'Password phải lớn hơn 4 kí tự.',
                'email.unique' => 'Email này đã tồn tại.',
                'ten.required' => 'Tên không được để trống',
                'password.required' => 'Password không được để trống',
            ]);
        $data = array();
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['roles_id'] = $request->roles;
        $user = DB::table('users')->where(['del_flg'=>0],['id'=>$id])->get();
        $current_password = $request->input('password_cu');
        $new_password = $request->input('password_moi');
        $new_repassword = $request->input('repassword');
        if (!Hash::check($current_password, $user->password)) {
            $notification = array(
                'messege' => 'Password cũ không đúng',
                'alert-type' => 'error'
            );
            return back()->with($notification)->withInput();
        } else {
            if ($new_password == $new_repassword) {
                $data['password'] = bcrypt($new_password);
                $data['roles_id'] = $request->roles;
                $userUpdate = DB::table('users')->where('id', $id)->update($data);
                if ($userUpdate) {
                    $notification = array(
                        'messege' => 'Sua user thành công',
                        'alert-type' => 'success'
                    );
                    return Redirect()->route('user.list')->with($notification);
                } else {

                    $notification = array(
                        'messege' => 'Lỗi',
                        'alert-type' => 'error'
                    );
                    return redirect()->back()->with($notification)->withInput();
                }
            } else {
                $notification = array(
                    'messege' => 'Mật khẩu mới không khớp',
                    'alert-type' => 'error'
                );
                return Redirect()->route('user.list')->with($notification)->withInput();
            }
        }
    }

    public function getXoa($id)
    {
        $userDel = Auth::user();
        if ($userDel->can('delete', User::class)) {
            $user = DB::table('users')->where(['del_flg'=>0],['id'=>$id])->get();
            if ($userDel->roles_id == 2 && $user->roles_id < 3) {
                $notification = array(
                    'messege' => 'Không có quyền',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            } else {
                if ($user->delete()) {
                    $notification = array(
                        'messege' => 'Xoa user thành công',
                        'alert-type' => 'success'
                    );
                    return redirect()->route('user.list')->with($notification);
                } else {

                    $notification = array(
                        'messege' => 'Lỗi',
                        'alert-type' => 'error'
                    );
                }
            }


        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->route('user.list')->with($notification);
        }

    }
}
