<?php

namespace App\Http\Controllers;

use App\LoaiTin;
use Illuminate\Http\Request;
use App\TheLoai;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function getLoaiTin($idTheLoai){
        $loaiTin=LoaiTin::where('idTheLoai',$idTheLoai)->get();
        foreach ($loaiTin as $lt)
        {
            echo "<option value='".$lt->id."'>".$lt->Ten."</option>";
        }
    }


//    public function getMenu(){
//        $menu='<ul>
//                            <li class="active"><a href="front/home" class="has dropdown-toggle">Trang chủ</a>
//                            </li>';
//        $theLoai=TheLoai::all();
//        foreach ($theLoai as $the_loai){
//            $menu.='<li><a href="theloai/'.$the_loai->id.'" class="has dropdown-toggle">'.$the_loai->Ten.'</a>';
//            $loai_tin=DB::table('loaitin')->where(['idTheLoai'=>$the_loai->id])->get();
//            // print_r($loai_tin);
//            if($loai_tin){
//                $menu.=' <ul class="sub-menu">';
//                foreach ($loai_tin as $item){
//                    $menu.='<li><a href="loaitin/'.$item->id.'">'.$item->Ten.'</a></li>';
//                }
//                $menu.='</ul></li>';
//            }
//            else{
//                $menu.='</li>';
//            }
//
//        }
//        $menu.='</ul>';
//        echo $menu;
//    }

}
