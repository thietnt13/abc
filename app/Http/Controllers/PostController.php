<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\User;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use stdClass;

class PostController extends Controller
{
    public function getList()
    {
        $user = Auth::User();
        $listCtegory = Category::all();
        $catUser = DB::table('user_category')->select('user_category.category_id')->where('user_id', $user->id)->get()->keyBy('category_id')->toArray();
        if ($user->roles_id == 3) {
            $post = DB::table('post')
                ->join('category', 'post.category_id', '=', 'category.id')
                ->join('users', 'post.user_id', '=', 'users.id')
                ->select('post.*', 'category.name as category', 'users.name as user_name')
                ->where(['post.user_id', '=', $user->id],['del_flg','=',0])
                ->paginate(4);
        }
        if($user->roles_id == 2)
        {
            $data=arrayCategoryUser($listCtegory,$catUser);
            $post = DB::table('post')
                ->join('category', 'post.category_id', '=', 'category.id')
                ->join('users', 'post.user_id', '=', 'users.id')
                ->select('post.*', 'category.name as category', 'users.name as user_name')
                ->whereIn('post.category_id',$data)->where('post.del_flg','=',0)
                ->paginate(4);
        }
        else{
            $post = DB::table('post')
                ->join('category', 'post.category_id', '=', 'category.id')
                ->join('users', 'post.user_id', '=', 'users.id')
                ->select('post.*', 'category.name as category', 'users.name as user_name')
                ->where('post.del_flg','=',0)
                ->paginate(4);
        }

        return view('admin.post.list', ['post' => $post]);
    }

    public function getThem()
    {
        $user = Auth::user();
        $category = Category::all();

        $catUser = DB::table('user_category')
            ->select('user_category.category_id')
            ->where('user_id', $user->id)
            ->get()
            ->keyBy('category_id')
            ->toArray();

        return view('admin.post.them', ['category' => $category, 'categoryUser' => $catUser,'user'=>$user]);
    }

    public function postThem(Request $request)
    {
        $this->validate($request,
            [
                'category' => 'required',
                'title' => 'required',
                'thumbnail' => 'required',
                'contentpost' => 'required',
            ],
            [
                'category.required' => 'Hãy chọn vào category',
                'title.required' => 'Tiêu đề không được để trống',
                'thumbnail.required' => 'Thumbnail không được để trống',
                'contentpost.required' => 'Content không được để trống',
            ]);
        $user = Auth::user();
        $catUser = DB::table('user_category')->select('user_category.category_id')->where('user_id', $user->id)->get()->keyBy('category_id')->toArray();
        $lstCategory = Category::all();
        if ($this->createPost($user, $request, $lstCategory, $catUser)) {
            $post = new Post();
            $post->category_id = $request->category;
            $post->title = $request->title;
            if ($request->hasFile('thumbnail')) {
                $fileThumbnail = $request->file('thumbnail');
                $name = $fileThumbnail->getClientOriginalName();
                $nameThumbnail = str_random(4) . '_' . $name;
                while (file_exists("home/tintuc/" . $nameThumbnail)) {
                    $nameThumbnail = str_random(4) . "_" . $name;
                }
                $fileThumbnail->move("home/tintuc", $nameThumbnail);
                $post->thumbnail = $nameThumbnail;
            } else {
                $post->thumbnail = '';
            }
            if ($request->hasFile('image')) {
                $fileAnh = $request->file('image');
                $name = $fileAnh->getClientOriginalName();
                $nameAnh = str_random(4) . '_' . $name;
                while (file_exists("home/tintuc/" . $nameAnh)) {
                    $nameAnh = str_random(4) . "_" . $name;
                }
                $fileAnh->move("home/tintuc", $nameAnh);
                $post->image = $nameAnh;
            } else {
                $post->image = '';
            }
            $post->user_id = $user->id;
            $post->content = $request->contentpost;
            $post->status = $request->rdbstatus;
            if ($post->save()) {
                $notification = array(
                    'messege' => 'Thêm bài viết thành công',
                    'alert-type' => 'success'
                );
                return redirect()->route('post.list')->with($notification);
            } else {
                $notification = array(
                    'messege' => 'Lỗi',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }
        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification)->withInput();
        }
    }

    public function getSua($id)
    {
        $user = Auth::user();
        $post = Post::findOrFail($id);
        $category = Category::all();
        $categoryUser = DB::table('user_category')->select('category_id')->where('user_id', $user->id)->get()->keyBy('category_id')->toArray();
        if ($this->permissionPost($user, $post->category_id, $category, $categoryUser, $post)) {
            return view('admin.post.sua', ['post' => $post,'categoryUser'=>$categoryUser, 'category' => $category,'user'=>$user]);
        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function postSua($id, Request $request)
    {
        $this->validate($request,
            [
                'category' => 'required',
                'title' => 'required',
                'contentpost' => 'required',
            ],
            [
                'category.required' => 'Hãy chọn vào category',
                'title.required' => 'Tiêu đề không được để trống',
                'contentpost.required' => 'Content không được để trống',
            ]);
        $postEdit = Post::findOrFail($id);
        $user = Auth::user();
        $category = Category::all();
        $categoryUser = DB::table('user_category')->select('category_id')->where('user_id', $user->id)->get()->keyBy('category_id')->toArray();
        if ($this->updatePost($user, $request, $category, $categoryUser)) {
            $data = array();
            $data['category_id'] = $request->category;
            $data['title'] = $request->title;
            if ($request->hasFile('thumbnail')) {
                $fileThumbnail = $request->file('thumbnail');
                $duoi = $fileThumbnail->getClientOriginalExtension();
                if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg') {
                    $notification = array(
                        'messege' => 'Bạn chỉ được chọn đuôi file ảnh jpg, png, jpeg',
                        'alert-type' => 'error'
                    );
                    return redirect()->back()->with($notification);
                }
                $name = $fileThumbnail->getClientOriginalName();
                $nameThumbnail = str_random(4) . '_' . $name;
                while (file_exists("home/tintuc/" . $nameThumbnail)) {
                    $nameThumbnail = str_random(4) . "_" . $name;
                }
                unlink("home/tintuc/" . $postEdit->thumbnail);
                $fileThumbnail->move("home/tintuc", $nameThumbnail);
                $data['thumbnail'] = $nameThumbnail;
            } else {
                $data['thumbnail'] = $postEdit->thumbnail;
            }
            if ($request->hasFile('image')) {
                $fileAnh = $request->file('image');
                $name = $fileAnh->getClientOriginalName();
                $nameAnh = str_random(4) . '_' . $name;
                while (file_exists("home/tintuc/" . $nameAnh)) {
                    $nameAnh = str_random(4) . "_" . $name;
                }
                $fileAnh->move("home/tintuc", $nameAnh);
                $data['image'] = $nameAnh;
            } else {
                $data['image'] = '';
            }
            $data['user_id'] = $user->id;
            $data['content'] = $request->contentpost;
            $data['status'] = $request->rdbstatus;
            $tintucUpdate = DB::table('post')->where('id', $id)->update($data);
            if ($tintucUpdate) {
                $notification = array(
                    'messege' => 'Sửa post thành công',
                    'alert-type' => 'success'
                );
                return redirect()->route('post.list')->with($notification);
            } else {
                $notification = array(
                    'messege' => 'Lỗi',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }
        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }

    public function getXoa($id)
    {
        $postDelete = Post::findOrFail($id);
        $user = Auth::user();
        $category = Category::all();
        $categoryUser = DB::table('user_category')->select('category_id')->where('user_id', $user->id)->get()->keyBy('category_id')->toArray();
        if ($this->permissionPost($user, $postDelete->category_id, $category, $categoryUser, $postDelete)) {
            $postUpdate = DB::table('post')->where('id',$id)->update(['del_flg'=>1]);
            if ($postUpdate) {
                $notification = array(
                    'messege' => 'Xóa post thành công',
                    'alert-type' => 'success'
                );
                return redirect()->route('post.list')->with($notification);
            } else {
                $notification = array(
                    'messege' => 'Lỗi',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }
        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }
}
