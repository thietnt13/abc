<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function DangNhap(){
        if(Auth::check()){
            view()->share('user_login',Auth::user());
        }
    }

    //----Permission Post
    public function createPost($user, $request, $listCategory, $categoryUser)
    {
        if ($user->roles_id == 3) {
            if($request->status == 1){
                return false;
            }
            return checkUserCategory($listCategory,$request->category,$categoryUser);
        }
        else if($user->roles_id == 2){
            return checkUserCategory($listCategory,$request->category,$categoryUser);
        }
        return true;
    }

    public function permissionPost($user, $category_id,$listCtegory,$categoryUser,$post){
        if($user->roles_id == 1){
            return true;
        }
        if($user->roles_id == 2){
            return checkUserCategory($listCtegory, $category_id,$categoryUser);
        }
        if($user->roles_id == 3){
            return $user->id == $post->user_id;
        }
    }

    public function updatePost($user, $request, $listCategory, $categoryUser)
    {
        if ($user->roles_id == 3) {
            if($request->status == 1){
                return false;
            }
            else {
                return checkUserCategory($listCategory, $request->category, $categoryUser);
            }
        }
        else if($user->roles_id == 2){
            return checkUserCategory($listCategory,$request->category,$categoryUser);
        }
        return true;
    }


    //------Permission Category
    public function createCategory($user,$listCategory,$request,$catUser){
        if($user->roles_id == 1){
            return true;
        }
        else if($user->roles_id == 2){
           return checkUserCategory($listCategory, $request->parent_cat, $catUser);
        }
    }

    public function permissionCategory($user,$category, $listCategory, $categoryUser){
        if($user->roles_id == 1){
            return true;
        }
        if($user ->roles_id == 2){
            return checkUserCategory($listCategory, $category, $categoryUser);
        }
        else{
            return false;
        }
    }

    //-----------Permission user

    public function createUser($user, $request){
        if($user->roles_id == 3){
            return false;
        }
        if($user->roles_id == 2 ){
            return $request->roles == 3;
        }
        return true;
    }
    public function updateUser($user,$user_id){
        if($user->roles_id == 1){
            return true;
        }
        else{
            return $user->id == $user_id;
        }
    }


}
