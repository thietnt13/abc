<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Excel;
use Mail;
use Session;
use phpDocumentor\Reflection\Type;

class StatisticController extends Controller
{
    public function getDashboard(){
        $allPost = DB::table('post')
            ->join('category','post.category_id','=','category.id')
            ->select('post.*','category.name as categoryName')
            ->where('post.del_flg','=',0)
            ->get();
        $publicPost = DB::table('post')
            ->join('category','post.category_id','=','category.id')
            ->select('post.*','category.name as categoryName')
            ->where('post.status','=',1)
            ->get();
        $deletePost = DB::table('post')
            ->join('category','post.category_id','=','category.id')
            ->select('post.*','category.name as categoryName')
            ->where('post.del_flg','=',1)
            ->get();

        return view('admin.statistic.dashboard',['allPost'=>$allPost,'publicPost'=>$publicPost,'deletePost'=>$deletePost]);
    }

    public function downloadExcelFile(){
        $allPost = DB::table('post')
            ->join('category','post.category_id','=','category.id')
            ->select('post.*','category.name as categoryName')
            ->where('post.del_flg','=',0)
            ->get();
        $publicPost = DB::table('post')
            ->join('category','post.category_id','=','category.id')
            ->select('post.*','category.name as categoryName')
            ->where('post.status','=',1)
            ->get();
        $deletePost = DB::table('post')
            ->join('category','post.category_id','=','category.id')
            ->select('post.*','category.name as categoryName')
            ->where('post.del_flg','=',1)
            ->get();
//        print_r($allPost);die();

        $typePost = [
            ['STT','Name','So luong'],
            ['1','Tất cả bài viết',count($allPost)],
            ['2','Bài viết public',count($publicPost)],
            ['3','Bài viết xóa',count($deletePost)],
        ];
       /* $typePost['allPost']=count($allPost);
        $typePost['publicPost'] = count($publicPost);
        $typePost['deletePost'] = count($deletePost);*/
       $this->sendMail($typePost);
        Excel::create('Statistic', function($excel) use ($typePost) {
            $excel->setTitle('Statistic');
            $excel->sheet('Statistic', function($sheet) use ($typePost)
            {
                $sheet->fromArray($typePost,null,'A1',false,false);
            });
        })->download('xlsx');

    }

    public function sendMail($typePost){
//        print_r($typePost);die();
        $user = Auth::user();
        Mail::send('admin.statistic.sendmail', array('typePost'=>$typePost), function($message){
            $message->to('thietnt2@topica.edu.vn', 'Visitor')->subject('Visitor Feedback!');
        });
        Session::flash('flash_message', 'Send message successfully!');
        return redirect()->back();
    }
}
