<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\User_Category;
use App\User;
use Dotenv\Exception\ValidationException;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function getList()
    {

        //print_r(Auth::user()->id);die();
        $user = Auth::user();

        $category = DB::table('category')
            ->where('del_flg','=',0)
            ->paginate(10);
        return view('admin.category.list', ['cate' => $category]);
    }


    //------them category method get
    public function getThem()
    {
        $user = Auth::user();
        if ($user->can('create', Category::class)) {
            $catUser = DB::table('user_category')
                ->select('user_category.category_id')
                ->where(['user_id'=> $user->id],['del_flg'=>0])
                ->get()
                ->keyBy('category_id')
                ->toArray();
            $category = DB::table('category')->where('del_flg','=',0)->get();
            $parent = DB::table('category')->where('del_flg','=',0)->get();
            $listUser = DB::table('users')->where('del_flg','=',0)->get();
            return view('admin.category.them', ['parent' => $parent, 'listUser' => $listUser,'category' => $category, 'categoryUser' => $catUser,'user'=>$user]);
        } else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }


    //-------------Them category method post
    public function postThem(Request $request)
    {
        $user = Auth::user();
        $listCategory = DB::table('category')
            ->where('del_flg','=',0)
            ->get();
        $catUser = DB::table('user_category')
            ->select('category_id')
            ->where(['user_id'=> $user->id],['del_flg'=>0])
            ->get()
            ->keyBy('category_id'
            )->toArray();

        if (checkUserCategory($listCategory, $request->parent_cat, $catUser) || $user->roles_id == 1) {

            $lstUser = $request->input('user');
            $this->validate($request,
                [
                    'name' => 'required|unique:category',
                    'user' => 'required'
                ],
                [
                    'name.required' => 'Tên không được để trống',
                    'name.unique' => 'Tên này đã tồn tại',
                    'user.required' => 'User không được để trống',
                ]);

            DB::beginTransaction();
            try {
                $newCategory = Category::create([
                    'name' => $request->name,
                    'parent_cat' => $request->parent_cat,
                    'del_flg'=>0
                ]);
                $idCategory = $newCategory->id;
                $dataInsert = [];

                foreach ($lstUser as $item ) {
                    $data = [
                        'user_id' => $item,
                        'category_id' => $idCategory,
                        'del_flg' => 0
                    ];
                    $dataInsert[] = $data;
                }
                User_Category::insert($dataInsert);

            } catch (ValidationException $e) {

                DB::rollBack();
                return Redirect()->back()
                    ->withErrors($e->getErrors())
                    ->withInput();

            } catch (\Exception $e) {
                DB::rollBack();
                throw $e;
            }
            DB::commit();

            $notification = array(
                'messege' => 'Thêm category thành công',
                'alert-type' => 'success'
            );
            return redirect()->route('category.list')->with($notification);

        } else {

            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification)->withInput();
        }


    }
    //-------------end Method


    //------------Sua Category method get
    public function getSua($id)
    {
        $user = Auth::user();
        $listCategory = DB::table('category')->where('del_flg','=',0)->get();
        $categoryUser = DB::table('user_category')->select('user_category.category_id')->where([['user_id','=',$user->id],['del_flg','=',0]])->get()->keyBy('category_id')->toArray();
        $userByCategory = DB::table('user_category')->select('user_category.user_id')->where([['category_id','=',$id],['del_flg','=',0]])->get()->keyBy('user_id')->toArray();
        $listUser = DB::table('users')->where('del_flg','=',0)->get();

        if($this->permissionCategory($user,$id,$listCategory,$categoryUser)){

            $category = DB::table('category')->where([['del_flg','=',0],['id','<>',$id]])->get();
            $categoryUser = DB::table('user_category')->select('category_id')->where(['user_id'=> $user->id],['del_flg'=>0])->get()->keyBy('category_id')->toArray();
            $cate = DB::table('category')->where([['del_flg','=',0],['id','=',$id]])->first();
            $parent_cate = DB::table('Category')->where([['id', '<>', $id],['del_flg','=',0]])->get()->toArray();
            return view('admin.category.sua', ['lstCategory'=>$category,'category' => $cate,'userByCategory'=>$userByCategory, 'parent_cat' => $parent_cate, 'listUser' => $listUser,'user'=>$user,'categoryUser'=>$categoryUser]);
        }
        else {
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }
    }
    //------------end method getSua

    public function postSua(Request $request, $id)
    {
        $this->validate($request,
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'Tên không được để trống',
            ]);
        $user = Auth::user();
        $listCategory = DB::table('category')->where('del_flg','=',0)->get();
        $catUser = DB::table('user_category')->select('category_id')->where(['user_id'=>$user->id],['del_flg'=>0])->get()->keyBy('category_id')->toArray();
        if (checkUserCategory($listCategory, $request->parent_cat, $catUser) || $user->roles_id == 1) {
            $updateDetail = array(
                'name'=>$request->name,
                'parent_cat'=>$request->parent_cat
            );
           DB::table('category')
               ->where('id',$id)
               ->update($updateDetail);
           DB::table('user_category')
               ->where('category_id',$id)
               ->update(['del_flg'=>1]);
            $lstUser = $request->input('user');
            $dataUpdate = [];
            foreach ($lstUser as $item) {
                $data = [
                    'user_id' => $item,
                    'category_id' => $id,
                    'del_flg'=>0
                ];
               $dataUpdate[] = $data;
            }
            User_Category::insert($dataUpdate);
            $notification = array(
                'messege' => 'Sửa category Thành công',
                'alert-type' => 'success'
            );
            return redirect()->route('category.list')->with($notification);
        }
        else{
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }


    }

    public function getXoa($id)
    {
        $parentCategory = Category::where(['parent_cat'=> $id],['del_flg'=>0])->count();
        $user = Auth::user();
        $listCategory = Category::all();
        $categoryUser = DB::table('user_category')->select('user_category.category_id')->where('user_id', $user->id)->get()->keyBy('category_id')->toArray();
        if($this->permissionCategory($user,$id,$listCategory,$categoryUser)){
            if ($parentCategory == 0) {
                $categoryDelete = DB::table('category')
                    ->where('id',$id)
                    ->update(['del_flg'=>1]);
                $postDelete = DB::table('post')
                    ->where('category_id',$id)
                    ->update(['del_flg'=>1]);
                if($categoryDelete){
                    $cateInCategoryUser = DB::table('user_category')
                        ->where('category_id',$id)
                        ->update(['del_flg'=>1]);
                    $notification = array(
                        'messege' => 'Xoa category thành công',
                        'alert-type' => 'success'
                    );
                    return redirect()->route('category.list')->with($notification);
                }
                else{
                    $notification = array(
                        'messege' => 'Lỗi',
                        'alert-type' => 'error'
                    );
                    return redirect()->route('category.list')->with($notification);
                }

            } else {

                $notification = array(
                    'messege' => 'Lỗi',
                    'alert-type' => 'error'
                );
                return redirect()->route('category.list')->with($notification);
            }
        }
        else{
            $notification = array(
                'messege' => 'Không có quyền',
                'alert-type' => 'error'
            );
            return redirect()->back()->with($notification);
        }


    }
}
