<?php

namespace App\Http\Controllers;

use App\LoaiTin;
use App\Models\Category;
use App\Models\Post;
use App\TheLoai;
use App\TinTuc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHome()
    {
        $tinTuc = Post::all();
        $theLoai = Category::all();
        return view('front.home', ['tinTuc' => $tinTuc, 'theLoai' => $theLoai]);
    }

    public function getTheLoai($idTL)
    {
        $the_loai_by_id = DB::table('theloai')->where('id', $idTL)->get();
        $loai_tin_by_theloai = DB::table('loaitin')->where('idTheLoai', $idTL)->get();
        $tin_tuc_theloai = TinTuc::all();
        $tinTuc = TinTuc::all();
        $theLoai = TheLoai::all();
        $laoiTin = LoaiTin::all();
        return view('front.theloai', ['tinTuc' => $tinTuc, 'theLoai' => $theLoai, 'loaiTin' => $laoiTin,'loai_tin_by_theloai' => $loai_tin_by_theloai, 'the_loai_by_id' => $the_loai_by_id, 'tin_tuc_theloai' => $tin_tuc_theloai]);
    }

    public function getLoaiTin($idLT)
    {
        $tinTuc = TinTuc::all();
        $theLoai = TheLoai::all();
        $laoiTin = LoaiTin::all();
        $loai_tin_by_id=LoaiTin::find($idLT);
        $tintuc_by_loaitin=DB::table('tintuc')->where('idLoaiTin',$idLT)->get()->toArray();
//        print_r($tintuc_by_loaitin);die();
//        print_r($loai_tin_by_id->Ten);die();
        return view('front.loaitin',['tinTuc' => $tinTuc, 'theLoai' => $theLoai, 'loaiTin' => $laoiTin,'loaitin_by_id'=>$loai_tin_by_id]);
    }

    public function getTinTuc($idTT)
    {
        $tinTuc = TinTuc::all();
        $theLoai = TheLoai::all();
        $laoiTin = LoaiTin::all();
        $tintuc_by_id=TinTuc::find($idTT);
        return view('front.tintuc',['tintuc_by_id'=>$tintuc_by_id,'tinTuc' => $tinTuc, 'theLoai' => $theLoai, 'loaiTin' => $laoiTin]);
    }
}
