<?php

namespace App\Policies;

use App\User;
use App\Post;
use http\Env\Request;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\User $user
     * @param  \App\Post $post
     * @return mixed
     */
    public function view(User $user, Post $post)
    {
        //
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user, Request $request,listCategory $listCategory, CategoryUser $categoryUser)
    {
        if ($user->roles_id == 3) {
            if($request->status == 1){
                return false;
            }
        }
        else if($user->roles_id == 2){
            return checkUserCategory($listCategory,$request->category,$categoryUser);
        }
        return true;
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User $user
     * @param  \App\Post $post
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->roles_id < 3;
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User $user
     * @param  \App\Post $post
     * @return mixed
     */
    public function delete(User $user, Post $post, listCategory $listCategory, categoryUser $categoryUser)
    {
        if ($user->roles_id == 3) {
            return $post->user_id == $user->id;
        } else if ($user->roles_id == 2) {
            return checkUserCategory($listCategory, $post->category_id, $categoryUser);
        }
        else{
            return true;
        }
    }

    protected function getPermission($user, $post,$listCategory,$categoryUser){

    }
}
