<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::get('login', 'AdminController@getLogin')->name('login');
    Route::post('login', 'AdminController@postLogin');
    Route::get('signup', 'AdminController@getSignup');
    Route::post('signup', 'AdminController@postSignup');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AdminController@logout');
        Route::get('user', 'AdminController@user');
    });
});

Route::group(['prefix' => 'admin','middleware' => 'auth'], function () {

    Route::group(['prefix'=>'statistic'],function (){
        Route::get('dashboard','StatisticController@getDashboard')->name('dashboard');
        Route::get('Post/{post}','StatisticController@getPost');
        Route::get('ExportClient','StatisticController@ExportClient');
        Route::get('downloadExcelFile','StatisticController@downloadExcelFile')->name('downloadExcelFile');
    });
    Route::group(['prefix' => 'user'], function () {
        Route::get('list', 'UserController@getList')->name('user.list');
        Route::get('them', 'UserController@getThem');
        Route::post('them', 'UserController@postThem');
        Route::get('sua/{id}', 'UserController@getSua');
        Route::post('sua/{id}', 'UserController@postSua');
        Route::get('xoa/{id}', 'UserController@getXoa');
    });
    Route::group(['prefix' => 'category'], function () {
        Route::get('list', 'CategoryController@getList')->name('category.list');
        Route::get('them', 'CategoryController@getThem');
        Route::post('them', 'CategoryController@postThem');
        Route::get('sua/{id}', 'CategoryController@getSua');
        Route::post('sua/{id}', 'CategoryController@postSua');
        Route::get('xoa/{id}', 'CategoryController@getXoa');
    });

    Route::group(['prefix' => 'post'], function () {
        Route::get('list', 'PostController@getList')->name('post.list');
        Route::get('them', 'PostController@getThem');
        Route::post('them', 'PostController@postThem');
        Route::get('xoa/{id}', 'PostController@getXoa');
        Route::get('sua/{id}', 'PostController@getSua');
        Route::post('sua/{id}', 'PostController@postSua');
    });

    Route::group(['prefix' => 'ajax'], function () {
        Route::get('loaitin/{idTheLoai}', 'AjaxController@getLoaiTin');
        Route::get('menu', 'AjaxController@getMenu');
    });

});
Route::group(['prefix'=>'front'],function (){
    Route::get('theloai/{id}','HomeController@getTheLoai');
    Route::get('loaitin/{id}','HomeController@getLoaiTin');
    Route::get('tintuc/{id}','HomeController@getTinTuc');
    Route::get('home', 'HomeController@getHome')->name('home');
});


Route::get('api/blog','BlogApi@index');
