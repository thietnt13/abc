@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Category
                        <small>Danh sách</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr align="center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Parent_Category</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cate as $item)
                    <tr class="odd gradeX" align="center">
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>
                            @if($item->parent_cat == 0)
                                {!!"None"!!}
                            @else
                                <?php
                                    $parent=DB::table('category')->where('id',$item->parent_cat)->first();
                                    echo $parent->name;
                                ?>
                            @endif
                        </td>
                        <td class="center">
                            <a href="admin/category/sua/{{$item->id}}" class="btn btn-info"> Sửa <i class="fa fa-pencil fa-fw"></i></a>
                            <a href="admin/category/xoa/{{$item->id}}" class="btn btn-danger"> Xóa <i class="fa fa-trash-o  fa-fw"></i></a>
                        </td>
                    </tr>
                  @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                {{$cate->links()}}
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

@endsection()