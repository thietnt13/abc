@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Category
                        <small>Sửa</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    <form action="admin/category/sua/{{$category->id}}" method="POST">
                        {{csrf_field()}}
                        <div class="form-group col-md-12">
                            <label  class="col-md-3 control-label">Tên (<span style="color: red">*</span>)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input class="form-control" name="name" placeholder="Tên" value="{{$category->name}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">Parent_Category</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="parent_cat" >
                                        <option value="0">--Select category--</option>
                                        <?php
                                        if($user->roles_id == 1){
                                            dsCategory($lstCategory,0,'',$category->parent_cat);
                                        }
                                        else{
                                            listCategoryByUser($lstCategory,0,'',$categoryUser,$category->parent_cat);
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">User (<span style="color: red">*</span>)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select id="user" multiple="multiple" class="form-control" name="user[]" >
                                        @foreach($listUser as $item)
                                            @if(array_key_exists($item->id,$userByCategory))
                                                <option value="{{$item->id}}" selected="'selected">{{$item->name}}</option>
                                            @else
                                                <option value="{{$item->id}}" >{{$item->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Sửa</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                        </form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@endsection()
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#user').multiselect({
                buttonWidth: '400px'
            });
        });
    </script>
@endsection
