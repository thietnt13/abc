@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tin tức
                        <small>Danh sách</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <table class="table table-bordered table-hover">
                    <thead style="background-color: #337ab7;color: white;">
                    <tr align="center">
                        <th>ID</th>
                        <th>title</th>
                        <th>Thumbnail</th>
                        <th>Status</th>
                        <th>Author</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($post as $tin_tuc)
                        <tr class="odd gradeX" align="center">
                            <td>{{$tin_tuc->id}}</td>
                            <td>{{$tin_tuc->title}}</td>
                            <td><img width="100px" src="home/tintuc/{{$tin_tuc->thumbnail}}"></td>
                            <td>{{$tin_tuc->status == 1?'Publisc':'Draft'}}</td>
                            <td>{{$tin_tuc->user_name}}</td>
                            <td>{{$tin_tuc->category}}</td>
                            <td class="center">
                                <a href="admin/post/info/{{$tin_tuc->id}}" class="btn btn-success" >Chi tiết</a>
                                <a href="admin/post/sua/{{$tin_tuc->id}}" class="btn btn-info" > Sửa</a>
                                <a href="admin/post/xoa/{{$tin_tuc->id}}" class="btn btn-danger"> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                {{$post->links()}}
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

@endsection()