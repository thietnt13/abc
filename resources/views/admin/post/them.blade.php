@extends('admin.master')
@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Post
                        <small>Thêm</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-12" style="padding-bottom:120px">
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>

                    @endif
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <form action="admin/post/them" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group col-md-12">
                            <label class="control-label col-md-2">Thể loại (<span style="color: red">*</span>)</label>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="category" >
                                        <option value="">--Select category--</option>
                                        <?php
                                            if($user->roles_id == 1){
                                                dsCategory($category,0,'',0);
                                            }
                                            else{
                                                listCategoryByUser($category,0,'',$categoryUser);
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label">Tiêu đề (<span style="color: red">*</span>)</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input class="form-control col-md-10" value="{{old('title')}}" name="title" placeholder="Nhập vào tiêu đề" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label">Thumbnail (<span style="color: red">*</span>)</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <input type="file" class="form-control" value="{{old('thumbnail')}}" name="thumbnail"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label">Nội dung (<span style="color: red">*</span>)</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <textarea id="demo" name="contentpost" value="{{old('contentpost')}}" class="ckeditor"></textarea>
                                </div>
                            </div>
                        </div>
                       {{-- <div class="form-group col-md-12">
                            <label class="col-md-2 control-label">Hình ảnh</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <input type="file" class="form-control" value="{{old('image')}}" rows="3" name="image"/>
                                </div>
                            </div>
                        </div>--}}
                        <div class="form-group col-md-12">
                            <label class="col-md-2 control-label">Status</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    @if(old('rdbstatus') == 1)
                                        <label class="radio-inline">
                                            <input name="rdbstatus" value="1" checked="checked" type="radio">Public
                                        </label>
                                        <label class="radio-inline">
                                            <input name="rdbstatus" value="0" type="radio">Draft
                                        </label>
                                    @else
                                        <label class="radio-inline">
                                            <input name="rdbstatus" value="1"  type="radio">Public
                                        </label>
                                        <label class="radio-inline">
                                            <input name="rdbstatus" value="0" checked="checked" type="radio">Draft
                                        </label>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3 col-md-push-9">
                            <button type="submit" class="btn btn-success">Post Add</button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>

                    </form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

@endsection()
{{--
@section('script')
    <script>
        $(document).ready(function () {
            $('#theLoai').change(function () {
                var idTheLoai=$(this).val();
                $.get("admin/ajax/loaitin/"+idTheLoai,function (data) {
                    $('#loaiTin').html(data);
                })
            })
        })
    </script>
@endsection--}}
