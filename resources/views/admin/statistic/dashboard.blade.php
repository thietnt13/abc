@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{count($allPost)}}</div>
                                    <div>Bài viết!</div>
                                </div>
                            </div>
                        </div>
                        <a onclick="showTable({{$allPost}})">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tag fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{count($publicPost)}}</div>
                                    <div>Bài viết public!</div>
                                </div>
                            </div>
                        </div>
                        <a onclick="showTable({{$publicPost}})">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-trash-o  fa-fw fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{count($deletePost)}}</div>
                                    <div>Bài viết bị xóa!</div>
                                </div>
                            </div>
                        </div>
                        <a onclick="showTable({{$deletePost}})">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <button id="excel" class="btn btn-success">Export excel</button>
                </div>
            </div>
            <div class="row">
                <table id="table_post" class="table table-bordered table-hover">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        function showTable(post) {
            var xml = '<thead >\n' +
                '                    <tr align="center">\n' +
                '                        <th>ID</th>\n' +
                '                        <th>title</th>\n' +
                '                        <th>Thumbnail</th>\n' +
                '                        <th>Status</th>\n' +
                '                        <th>Category</th>\n' +
                '                        <th>Action</th>\n' +
                '                    </tr>\n' +
                '                    </thead>\n' +
                '                    <tbody >';
            post.forEach(function (item) {
                xml += '<tr class="odd gradeX" align="center">'
                        +'<td>'+item['id']+'</td>'
                        +'<td>'+item['title']+'</td>'
                        +'<td><img width="100px" src="home/tintuc/'+item['thumbnail']+'"></td>'
                        +'<td>'+item['status']+'</td>'
                        +'<td>'+item['categoryName']+'</td>'
                        +'<td class="center">'
                            +'<a href="admin/post/info/'+item['id']+'" class="btn btn-success">Chi tiết</a>'
                            +'<a href="admin/post/sua/'+item['id']+'" class="btn btn-info">Sửa</a>'
                            +'<a href="admin/post/xoa/'+item['id']+'" class="btn btn-danger">Xóa</a>'
                        +'</td>'
                    +'</tr>'

            })
            $('#table_post').html(xml);
        }

        $('#excel').on('click',function(){

            var query = {
                location: $('#location').val(),
                area: $('#area').val(),
                booth: $('#booth').val()
            }


            var url = "{{URL::to('admin/statistic/downloadExcelFile')}}?" + $.param(query)

            window.location = url;
        });
    </script>
@endsection
