@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Thống kê
                        <small>Danh sách</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-12" >
                    <button style="float: right;" class="btn btn-success">Dowload Excel</button>
                </div>
                <div class="col-lg-12">
                    <table class="table table-bordered table-hover">
                        <thead style="background-color: #337ab7;color: white;">
                        <tr align="center">
                            <th>STT</th>
                            <th>Loại bài viết</th>
                            <th>Số lượng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($typePost as $key => $val)
                            <tr class="odd gradeX" align="center">
                                <td>{{$key}}</td>
                                <td>{{$val[0]}}</td>
                                <td>{{$val[1]}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>

@endsection()
@section('script')
@endsection