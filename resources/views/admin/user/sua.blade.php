@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User
                        <small>Sửa</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    <form action="admin/user/sua/{{$user->id}}" method="POST">
                        {{csrf_field()}}
                        <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">Tên (<span style="color: red">*</span>)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input class="form-control" name="name" placeholder="Tên" value="{{$user->name}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">Email (<span style="color: red">*</span>)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input class="form-control" name="email" placeholder="email"
                                           value="{{$user->email}}"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">Password cũ (<span style="color: red">*</span>)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="password" class="form-control" name="password_cu"
                                           placeholder="Password"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">Password mới (<span style="color: red">*</span>)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="password" class="form-control" name="password_moi"
                                           placeholder="Password"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">RePassword (<span style="color: red">*</span>)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input class="form-control" type="password" name="repassword"
                                           placeholder="Nhập lại password"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">Roles (<span style="color: red">*</span>)</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select class="form-control" name="roles">
                                        @foreach($roles as $roles_user)
                                            <option @if($user->roles_id == $roles_user->id) {{"selected"}} @endif value="{{$roles_user->id}}">{{$roles_user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                       {{-- <div class="form-group col-md-12">
                            <label class="col-md-3 control-label">Category</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <select id="category" multiple="multiple" class="form-control" name="category[]" >
                                        @foreach($listCategory as $item)
                                            @if(array_key_exists($item->id,$categoryByUser))
                                                <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                            @else
                                                <option value="{{$item->id}}" >{{$item->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>--}}
                        <button type="submit" class="btn btn-success">Sửa</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@endsection()
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#category').multiselect({
                buttonWidth: '400px'
            });
        });
    </script>
@endsection