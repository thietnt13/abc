
<!-- /.navbar-static-side -->

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href=""><i class="fa fa-user"></i> Dashboard<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="admin/statistic/dashboard"><i class="fa fa-list"></i> Dashboard </a>
                    </li>
                    <li>
                        <a href="admin/statistic/ExportClient"><i class="fa fa-plus-circle"></i> Export File</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-user"></i> User<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="admin/user/list"><i class="fa fa-list"></i> List</a>
                    </li>
                    <li>
                        <a href="admin/user/them"><i class="fa fa-plus-circle"></i> Add</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href=""><i class="fa fa-bar-chart-o fa-fw"></i> Category <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="admin/category/list"><i class="fa fa-list"></i> List</a>
                    </li>
                    <li>
                        <a href="admin/category/them"><i class="fa fa-plus-circle"></i> Add</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href=""><i class="fa fa-newspaper-o"></i> Post<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="admin/post/list"><i class="fa fa-list"></i> List</a>
                    </li>
                    <li>
                        <a href="admin/post/them"><i class="fa fa-plus-circle"></i> Add</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>




        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>