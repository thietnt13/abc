<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="row sidebar-area">
        <div class="recent-post-area hot-news">
                <div class="col-sm-12 heading" style=" margin-bottom: 5px;   padding-left: 0px;">
                    <h3 class="title-bg"><span>Xây dựng</span></h3>
                </div>
            <ul class="news-post">
                @foreach($tinTuc as $tin)
                    <li>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content">
                                <div class="item-post">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 paddimg-right-none">
                                            <a href="tintuc/{{$tin->id}}"><img src="home/tintuc/{{$tin->Hinh}}" alt="" title="News image"></a>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <h4><a href="tintuc/{{$tin->id}}">{{$tin->TieuDe}}</a></h4>
                                            <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> {{$tin->created_at}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>
</div>