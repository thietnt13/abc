<div class="header-bottom-area container" style="margin-bottom: 15px" id="sticky">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                <div class="navbar-header">
                    <div class="col-sm-8 col-xs-8 padding-null">
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="col-sm-4 col-xs-4 hidden-desktop text-right search">
                        <a href="#search-mobile" data-toggle="collapse" class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></a>
                        <div id="search-mobile" class="collapse search-box">
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </div>
                </div>
                <nav class="main-menu navbar-collapse collapse">
                        <ul>
                            <li class="active"><a href="front/home" class="has dropdown-toggle">Trang chủ</a>
                            </li>
                            <?php
                                $menuLevel1= \Illuminate\Support\Facades\DB::table('category')->where('parent_cat',0)->get();
                                ?>
                            @foreach ($menuLevel1 as $cateParent)
                                 <li><a href="theloai/{{$cateParent->id}}" class="has dropdown-toggle">{{$cateParent->name}}</a>
                                     <?php $menuLevel2=DB::table('category')->where(['parent_cat'=>$cateParent->id])->get(); ?>
                                     @if($menuLevel2)
                                         <ul class="sub-menu">
                                             @foreach ($menuLevel2 as $item)
                                                 <li><a href="theloai/{{$item->id}}">{{$item->name}}</a></li>
                                            @endforeach
                                         </ul>
                                    @endif
                                 </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            <div class="col-lg-2 col-md-2 col-sm-hidden col-xs-hidden text-right search hidden-mobile">
                <a href="#search" data-toggle="collapse" class="search-icon"><i class="fa fa-search" aria-hidden="true"></i></a>
                <div id="search" class="collapse search-box">
                    <input type="text" class="form-control" placeholder="Search...">
                </div>
            </div>
        </div>
    </div>
</div>
