<div class="col-sm-12 tinthuong" id="id_tinthuong">
    <div class='col-sm-12 category-tinthuong' id='id_category_tinthuong'>
        <span class='span-tinthuong' id="id_span_tinthuong">TIN KHAC</span>
    </div>
    <div class="form-tinthuong" id="id_form_tinthuong">

        @if(!empty($tinTuc))
            <?php $i = 0;?>
            @foreach($tinTuc as $tin_tuc)
                <?php $i++;?>
                <div class="col-sm-12 content-tinthuong " id="id_content_tinthuong{{$i}}">
                    <div class="col-sm-12 item-tinthuong hvr-glow hvr-box-shadow-inset hvr-overline-from-center"
                         id="id_item_tinthuong{{$i}}">

                        <div class="col-sm-2 image-tinthuong" id="id_image_tinthuong{{$i}}">
                            <div class="row" id="id_row{{$i}}">
                                <a href="front/baiviet/{{$tin_tuc->id}}"
                                   id="ai{{$i}}">
                                        <img src="home/tintuc/{{$tin_tuc->Hinh}}"
                                             class="img-responsive img-category-tinthuong"
                                             alt=""/>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-10" id='id_detail_tinthuong{{$i}}'>
                            <div class="row title-tinthuong" id='id_title_tinthuong{{$i}}'>
                                <div class="col-sm-12 content-title-tinthuong"
                                     id="id_content-title-tinthuong{{$i}}">
                                    <a class="link" id="id_link"
                                       href="front/baiviet/{{$tin_tuc->id}}"><span class='span-bold-blue'>{{$tin_tuc->TieuDe}}</span></a>
                                </div>
                                <div class="col-sm-10 info3">
                                    <span class='span-small-font '>
                                            <?php
                                            $title = substr($tin_tuc->NoiDung, 0, 300);
                                            $pos = strrpos($title, " ");
                                            $title = substr($title, 0, $pos);
                                            print($title);
                                        ?>

                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end .product-item-->
            @endforeach
        @else
            <div class="col-sm-12"><span style="color:red">Không tìm thấy!</span></div>
        @endif
    </div>

</div>

<!--End .row-->

