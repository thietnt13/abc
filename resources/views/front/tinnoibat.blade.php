<div class="col-sm-9 col-md-9 tinnoibat" id="product_row">

    <div class="bodyContainer">
        <?php
            $tinNoiBat=$tinTuc->where('NoiBat',1)->sortByDesc('created_at')->take(4);
            //$tin1=$tinNoiBat->shift();
        ?>
        @if(!empty($tinNoiBat))
            <?php $i = 0;?>
            @foreach($tinNoiBat as $tin_noi_bat)
                <?php $i++;?>
                <div class="col-sm-12 content-tinnoibat" id="id_product_item{{$i}}">
                    <div class="col-sm-12 item-tinnoibathvr-glow hvr-overline-from-center">
                        <div class="col-sm-4 image-tinnoibat" id="id_image{{$i}}">
                            <div class="row ">
                                <a href="" id="ai{{$i}}">
                                    @if(!empty($tin_noi_bat->Hinh))
                                        <img src="home/tintuc/{{$tin_noi_bat->Hinh}}"
                                             class="img-responsive img-category-tinnoibat" alt="{{$tin_noi_bat->TieuDe}}"/>
                                    @else
                                        <img src="home/tintuc/{{$tin_noi_bat->Hinh}}" class="img-responsive img-category"
                                             alt="{{$tin_noi_bat->TieuDe}}"/>
                                    @endif
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-7 col-sm-push-1" id='title{{$i}}'>
                            <div class="row title-tinnoibat">
                                <div class="col-sm-12 info">
                                    @if(strlen($tin_noi_bat->TieuDe) <'75')
                                        <p id='p{{$i}}'><a href="front/chitietbaiviet/{{$tin_noi_bat->id}}"><span
                                                        class='span-bold-red'>{{$tin_noi_bat->TieuDe}}</span></a></p>
                                    @else
                                        <?php
                                        $title = substr($tin_noi_bat->TieuDe, 0, 70);
                                        $pos = strrpos($title, " ");
                                        $title = substr($title, 0, $pos);

                                        ?>
                                        <p id='p{{$i}}'><a href="front/chitietbaiviet/{{$tin_noi_bat->id}}"><span class='span-bold-red'>{{$title}}...</span></a></p>

                                    @endif


                                </div>
                            </div>
                            <div class="col-sm-10 info3">
                                    <span style="font-family: 'Arial Black, arial-black'">
                                            <?php
                                        $title = substr($tin_noi_bat->NoiDung, 0, 300);
                                        $pos = strrpos($title, " ");
                                        $title = substr($title, 0, $pos);
                                        print($title);
                                        ?>

                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end .product-item-->
            @endforeach
        @else
            <div class="col-sm-12"><span style="color:red">Không tìm thấy!</span></div>
        @endif
    </div>

</div>