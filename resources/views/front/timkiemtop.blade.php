<div class="home-search-box">
    <div class="tops">
        <h2>Công cụ tìm kiếm</h2>
    </div>
    <div class="contents">
        <!--begin tab-->
        <div class="tab">
            <div onclick="ShowTab(1);" class="divSearchAct" id="divTabRESale">
                <h3><a>
                        Nhà đất<br>bán
                    </a></h3>
            </div>
            <div style="display: none;" id="tab-line_1" class="tab-lines">
            </div>
            <div onclick="ShowTab(2);" class="divSearchInAct" id="divTabREBorrow">
                <h3><a>
                        Nhà đất<br>cho thuê
                    </a></h3>
            </div>
            <div id="tab-line_2" class="tab-lines" style="display: block;">
            </div>
            <div onclick="ShowTab(3);" class="divSearchInAct" id="divTabRESaler">
                <h3><a>
                        Tìm môi<br>giới</a></h3>
            </div>
        </div>
        <!--end tab-->
        <div id="divOfSeach">
            <div class="home-product-search">
                <div id="searchArea" style="overflow: hidden;">
                    <div class="comboboxs">
                        <div class="newicon" style="width: 195px; padding-left: 5px; border: 1px solid #ccc;">
                            <input type="text" id="txtAutoComplete" placeholder="Nhập địa điểm, vd: Sunrise City"
                                   style="position: relative; border: 0; width: 170px;" class="ui-autocomplete-input"
                                   autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
                        </div>
                        <div class="suggestTT" style="display: none;">
                            <div class="arr">
                                <img src="https://file4.batdongsan.com.vn/images/opt/greyarrow.png">
                            </div>
                            <div class="greencolor goiy"><strong>Gợi ý</strong></div>
                            <ul>
                                <li>Nhập tên tỉnh/thành phố, quận/huyện, phường/xã, đường/phố, dự án; ví dụ: Sunrise
                                    City
                                </li>
                                <li>Phải chọn các gợi ý chúng tôi đề xuất bên dưới để kết quả chính xác</li>
                                <li>Nếu không nhập địa điểm ở đây, Quý vị có thể chọn lựa khu vực bằng các ô phía dưới
                                    trong công cụ tìm kiếm này
                                </li>
                            </ul>
                            <div class="closeTT">
                                <img src="https://file4.batdongsan.com.vn/images/opt/close.png">
                            </div>
                        </div>
                    </div>
                    <div id="divCategoryRe" class="comboboxs advance-select-box">
<span class="select-text">
<span class="select-text-content" style="width: 175px;">--Chọn loại nhà đất--</span>
</span>
                        <input type="hidden" name="cboTypeRe" id="hdCboCatagoryRe" value="0">
                        <div id="divCatagoryReOptions" class="advance-select-options advance-options"
                             hddvalue="hdCboCatagoryRe" ddlid="divCategoryRe" style="">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li class="advance-options" vl="0">--Chọn loại nhà đất--</li>
                                <li class="advance-options" vl="324">- Bán căn hộ chung cư</li>
                                <li class="advance-options" vl="362">- Tất cả các loại nhà bán</li>
                                <li class="advance-options" vl="41">&nbsp;&nbsp;&nbsp;&nbsp;+ Bán nhà riêng</li>
                                <li class="advance-options" vl="325">&nbsp;&nbsp;&nbsp;&nbsp;+ Bán nhà biệt thự, liền
                                    kề
                                </li>
                                <li class="advance-options" vl="163">&nbsp;&nbsp;&nbsp;&nbsp;+ Bán nhà mặt phố</li>
                                <li class="advance-options" vl="361">- Tất cả các loại đất bán</li>
                                <li class="advance-options" vl="40">&nbsp;&nbsp;&nbsp;&nbsp;+ Bán đất nền dự án</li>
                                <li class="advance-options" vl="283">&nbsp;&nbsp;&nbsp;&nbsp;+ Bán đất</li>
                                <li class="advance-options" vl="44">- Bán trang trại, khu nghỉ dưỡng</li>
                                <li class="advance-options" vl="45">- Bán kho, nhà xưởng</li>
                                <li class="advance-options" vl="48">- Bán loại bất động sản khác</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divCity" class="comboboxs advance-select-box">
                        <span class="select-text"><input type="text" class="select-text-content"
                                                         value="--Chọn Tỉnh/Thành phố--"
                                                         placeholder="--Chọn Tỉnh/Thành phố--"
                                                         style="width: 175px;"></span>
                        <input type="hidden" name="cboCity" id="hdCboCity" value="CN">
                        <div id="divCityOptions" class="advance-select-options advance-options" hddvalue="hdCboCity"
                             ddlid="divCity" style="">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="CN" class="advance-options" style="min-width: 168px;">--Chọn Tỉnh/Thành phố--
                                </li>
                                <li vl="SG" class="advance-options" style="min-width: 168px;">Hồ Chí Minh</li>
                                <li vl="HN" class="advance-options" style="min-width: 168px;">Hà Nội</li>
                                <li vl="DDN" class="advance-options" style="min-width: 168px;">Đà Nẵng</li>
                                <li vl="BD" class="advance-options" style="min-width: 168px;">Bình Dương</li>
                                <li vl="DNA" class="advance-options" style="min-width: 168px;">Đồng Nai</li>
                                <li vl="KH" class="advance-options" style="min-width: 168px;">Khánh Hòa</li>
                                <li vl="HP" class="advance-options" style="min-width: 168px;">Hải Phòng</li>
                                <li vl="LA" class="advance-options" style="min-width: 168px;">Long An</li>
                                <li vl="QNA" class="advance-options" style="min-width: 168px;">Quảng Nam</li>
                                <li vl="VT" class="advance-options" style="min-width: 168px;">Bà Rịa Vũng Tàu</li>
                                <li vl="DDL" class="advance-options" style="min-width: 168px;">Đắk Lắk</li>
                                <li vl="CT" class="advance-options" style="min-width: 168px;">Cần Thơ</li>
                                <li vl="BTH" class="advance-options" style="min-width: 168px;">Bình Thuận</li>
                                <li vl="LDD" class="advance-options" style="min-width: 168px;">Lâm Đồng</li>
                                <li vl="TTH" class="advance-options" style="min-width: 168px;">Thừa Thiên Huế</li>
                                <li vl="KG" class="advance-options" style="min-width: 168px;">Kiên Giang</li>
                                <li vl="BN" class="advance-options" style="min-width: 168px;">Bắc Ninh</li>
                                <li vl="QNI" class="advance-options" style="min-width: 168px;">Quảng Ninh</li>
                                <li vl="TH" class="advance-options" style="min-width: 168px;">Thanh Hóa</li>
                                <li vl="NA" class="advance-options" style="min-width: 168px;">Nghệ An</li>
                                <li vl="HD" class="advance-options" style="min-width: 168px;">Hải Dương</li>
                                <li vl="GL" class="advance-options" style="min-width: 168px;">Gia Lai</li>
                                <li vl="BP" class="advance-options" style="min-width: 168px;">Bình Phước</li>
                                <li vl="HY" class="advance-options" style="min-width: 168px;">Hưng Yên</li>
                                <li vl="BDD" class="advance-options" style="min-width: 168px;">Bình Định</li>
                                <li vl="TG" class="advance-options" style="min-width: 168px;">Tiền Giang</li>
                                <li vl="TB" class="advance-options" style="min-width: 168px;">Thái Bình</li>
                                <li vl="BG" class="advance-options" style="min-width: 168px;">Bắc Giang</li>
                                <li vl="HB" class="advance-options" style="min-width: 168px;">Hòa Bình</li>
                                <li vl="AG" class="advance-options" style="min-width: 168px;">An Giang</li>
                                <li vl="VP" class="advance-options" style="min-width: 168px;">Vĩnh Phúc</li>
                                <li vl="TNI" class="advance-options" style="min-width: 168px;">Tây Ninh</li>
                                <li vl="TN" class="advance-options" style="min-width: 168px;">Thái Nguyên</li>
                                <li vl="LCA" class="advance-options" style="min-width: 168px;">Lào Cai</li>
                                <li vl="NDD" class="advance-options" style="min-width: 168px;">Nam Định</li>
                                <li vl="QNG" class="advance-options" style="min-width: 168px;">Quảng Ngãi</li>
                                <li vl="BTR" class="advance-options" style="min-width: 168px;">Bến Tre</li>
                                <li vl="DNO" class="advance-options" style="min-width: 168px;">Đắk Nông</li>
                                <li vl="CM" class="advance-options" style="min-width: 168px;">Cà Mau</li>
                                <li vl="VL" class="advance-options" style="min-width: 168px;">Vĩnh Long</li>
                                <li vl="NB" class="advance-options" style="min-width: 168px;">Ninh Bình</li>
                                <li vl="PT" class="advance-options" style="min-width: 168px;">Phú Thọ</li>
                                <li vl="NT" class="advance-options" style="min-width: 168px;">Ninh Thuận</li>
                                <li vl="PY" class="advance-options" style="min-width: 168px;">Phú Yên</li>
                                <li vl="HNA" class="advance-options" style="min-width: 168px;">Hà Nam</li>
                                <li vl="HT" class="advance-options" style="min-width: 168px;">Hà Tĩnh</li>
                                <li vl="DDT" class="advance-options" style="min-width: 168px;">Đồng Tháp</li>
                                <li vl="ST" class="advance-options" style="min-width: 168px;">Sóc Trăng</li>
                                <li vl="KT" class="advance-options" style="min-width: 168px;">Kon Tum</li>
                                <li vl="QB" class="advance-options" style="min-width: 168px;">Quảng Bình</li>
                                <li vl="QT" class="advance-options" style="min-width: 168px;">Quảng Trị</li>
                                <li vl="TV" class="advance-options" style="min-width: 168px;">Trà Vinh</li>
                                <li vl="HGI" class="advance-options" style="min-width: 168px;">Hậu Giang</li>
                                <li vl="SL" class="advance-options" style="min-width: 168px;">Sơn La</li>
                                <li vl="BL" class="advance-options" style="min-width: 168px;">Bạc Liêu</li>
                                <li vl="YB" class="advance-options" style="min-width: 168px;">Yên Bái</li>
                                <li vl="TQ" class="advance-options" style="min-width: 168px;">Tuyên Quang</li>
                                <li vl="DDB" class="advance-options" style="min-width: 168px;">Điện Biên</li>
                                <li vl="LCH" class="advance-options" style="min-width: 168px;">Lai Châu</li>
                                <li vl="LS" class="advance-options" style="min-width: 168px;">Lạng Sơn</li>
                                <li vl="HG" class="advance-options" style="min-width: 168px;">Hà Giang</li>
                                <li vl="BK" class="advance-options" style="min-width: 168px;">Bắc Kạn</li>
                                <li vl="CB" class="advance-options" style="min-width: 168px;">Cao Bằng</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divDistrict" class="comboboxs advance-select-box" title="">
                        <span class="select-text"><input type="text" class="select-text-content"
                                                         value="--Chọn Quận/Huyện--" placeholder="--Chọn Quận/Huyện--"
                                                         style="width: 175px;"></span>
                        <input type="hidden" name="cboDistrict" id="hdCboDistrict" value="0">
                        <div id="divDistrictOptions" class="advance-select-options advance-options"
                             hddvalue="hdCboDistrict" ddlid="divDistrict" style="">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="0" class="advance-options" style="min-width: 168px;">--Chọn Quận/Huyện--</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divArea" class="comboboxs advance-select-box">
<span class="select-text">
<span class="select-text-content" style="width: 175px;">--Chọn diện tích--</span>
</span>
                        <input type="hidden" id="hdCboArea" name="cboArea" value="-1">
                        <div id="divAreaOptions" class="advance-select-options advance-area-options advance-options"
                             minvalue="txtAreaMinValue" maxvalue="txtAreaMaxValue" unit="area" hddvalue="hdCboArea"
                             ddlid="divArea" style="">
                            <table class="header-options options">
                                <tbody>
                                <tr class="advance-options">
                                    <td class="advance-options">
                                        <input type="text" id="txtAreaMinValue" name="areamin" placeholder="từ"
                                               class="min-value advance-options" maxlength="6" numbersonly="true"
                                               decimal="false">
                                    </td>
                                    <td class="advance-options">
                                        <span class="advance-options">- </span>
                                    </td>
                                    <td class="advance-options">
                                        <input type="text" id="txtAreaMaxValue" name="areamax" placeholder="đến"
                                               class="max-value advance-options" maxlength="6" numbersonly="true"
                                               decimal="false">
                                    </td>
                                    <td class="advance-options">
                                        <span class="advance-options">m2</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="-1" class="advance-options" style="min-width: 168px;">--Chọn diện tích--</li>
                                <li vl="0" class="advance-options" style="min-width: 168px;">Chưa xác định</li>
                                <li vl="1" class="advance-options" style="min-width: 168px;">&lt;= 30 m2</li>
                                <li vl="2" class="advance-options" style="min-width: 168px;">30 - 50 m2</li>
                                <li vl="3" class="advance-options" style="min-width: 168px;">50 - 80 m2</li>
                                <li vl="4" class="advance-options" style="min-width: 168px;">80 - 100 m2</li>
                                <li vl="5" class="advance-options" style="min-width: 168px;">100 - 150 m2</li>
                                <li vl="6" class="advance-options" style="min-width: 168px;">150 - 200 m2</li>
                                <li vl="7" class="advance-options" style="min-width: 168px;">200 - 250 m2</li>
                                <li vl="8" class="advance-options" style="min-width: 168px;">250 - 300 m2</li>
                                <li vl="9" class="advance-options" style="min-width: 168px;">300 - 500 m2</li>
                                <li vl="10" class="advance-options" style="min-width: 168px;">&gt;= 500 m2</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divPrice" class="comboboxs advance-select-box">
<span class="select-text">
<span class="select-text-content" style="width: 175px;">--Chọn mức giá--</span>
</span>
                        <input type="hidden" id="hdCboPrice" name="cboPrice" value="-1">
                        <div id="divPriceOptions" class="advance-select-options advance-price-options advance-options"
                             minvalue="txtPriceMinValue" maxvalue="txtPriceMaxValue" unit="money" lblmin="lblPriceMin"
                             lblmax="lblPriceMax" hddvalue="hdCboPrice" ddlid="divPrice" style="">
                            <table class="header-options options">
                                <tbody>
                                <tr class="advance-options">
                                    <td class="advance-options">
                                        <input type="text" id="txtPriceMinValue" name="pricemin" placeholder="từ"
                                               class="min-value advance-options" maxlength="6" numbersonly="true"
                                               decimal="true">
                                        <div id="lblPriceMin"></div>
                                    </td>
                                    <td class="advance-options">
                                        <span class="advance-options">- </span></td>
                                    <td class="advance-options">
                                        <input type="text" id="txtPriceMaxValue" name="pricemax" placeholder="đến"
                                               class="max-value advance-options" maxlength="6" numbersonly="true"
                                               decimal="true">
                                        <div id="lblPriceMax"></div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="-1" class="advance-options">--Chọn mức giá--</li>
                                <li vl="0" class="advance-options">Thỏa thuận</li>
                                <li vl="1" class="advance-options">&lt; 500 triệu</li>
                                <li vl="2" class="advance-options">500 - 800 triệu</li>
                                <li vl="3" class="advance-options">800 triệu - 1 tỷ</li>
                                <li vl="4" class="advance-options">1 - 2 tỷ</li>
                                <li vl="5" class="advance-options">2 - 3 tỷ</li>
                                <li vl="6" class="advance-options">3 - 5 tỷ</li>
                                <li vl="7" class="advance-options">5 - 7 tỷ</li>
                                <li vl="8" class="advance-options">7 - 10 tỷ</li>
                                <li vl="9" class="advance-options">10 - 20 tỷ</li>
                                <li vl="10" class="advance-options">20 - 30 tỷ</li>
                                <li vl="11" class="advance-options">&gt; 30 tỷ</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divWard" class="comboboxs advance-select-box adv-search" title="" style="display: none;">
                        <span class="select-text"><input type="text" class="select-text-content"
                                                         value="--Chọn Phường/Xã--" placeholder="--Chọn Phường/Xã--"
                                                         style="width: 175px;"></span>
                        <input type="hidden" id="hdCboWard" name="cboWard" value="0">
                        <div id="divWardOptions" class="advance-select-options advance-options" hddvalue="hdCboWard"
                             ddlid="divWard" style="">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="0" class="advance-options" style="min-width: 168px;">--Chọn Phường/Xã--</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divStreet" class="comboboxs advance-select-box adv-search" title="" style="display: none;">
                        <span class="select-text"><input type="text" class="select-text-content"
                                                         value="--Chọn Đường/Phố--" placeholder="--Chọn Đường/Phố--"
                                                         style="width: 175px;"></span>
                        <input type="hidden" id="hdCboStreet" name="cboStreet" value="0">
                        <div id="divStreetOptions" class="advance-select-options advance-options" hddvalue="hdCboStreet"
                             ddlid="divStreet" style="">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="0" class="advance-options" style="min-width: 168px;">--Chọn Đường/Phố--</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divBedRoom" class="comboboxs advance-select-box adv-search" style="display: none;">
<span class="select-text">
<span class="select-text-content" style="width: 175px;">--Chọn số phòng ngủ--</span>
</span>
                        <input type="hidden" id="hdCboBedRoom" name="cboBedRoom" value="-1">
                        <div id="divBedRoomOptions" class="advance-select-options advance-options"
                             hddvalue="hdCboBedRoom" ddlid="divBedRoom" style="">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="-1" class="advance-options" style="min-width: 168px;">--Chọn số phòng ngủ--</li>
                                <li vl="0" class="advance-options" style="min-width: 168px;">Không xác định</li>
                                <li vl="1" class="advance-options" style="min-width: 168px;">1+</li>
                                <li vl="2" class="advance-options" style="min-width: 168px;">2+</li>
                                <li vl="3" class="advance-options" style="min-width: 168px;">3+</li>
                                <li vl="4" class="advance-options" style="min-width: 168px;">4+</li>
                                <li vl="5" class="advance-options" style="min-width: 168px;">5+</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divHomeDirection" class="comboboxs advance-select-box adv-search" style="display: none;">
<span class="select-text">
<span class="select-text-content" style="width: 175px;">--Chọn hướng nhà--</span>
</span>
                        <input type="hidden" id="hdCboHomeDirection" name="cboHomeDirection" value="-1">
                        <div id="divHomeDirectionOptions" class="advance-select-options advance-options"
                             hddvalue="hdCboHomeDirection" ddlid="divHomeDirection" style="">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="-1" style="min-width: 168px;">--Chọn hướng nhà--</li>
                                <li vl="0" class="advance-options" style="min-width: 168px;">KXĐ</li>
                                <li vl="1" class="advance-options" style="min-width: 168px;">Đông</li>
                                <li vl="2" class="advance-options" style="min-width: 168px;">Tây</li>
                                <li vl="3" class="advance-options" style="min-width: 168px;">Nam</li>
                                <li vl="4" class="advance-options" style="min-width: 168px;">Bắc</li>
                                <li vl="5" class="advance-options" style="min-width: 168px;">Đông-Bắc</li>
                                <li vl="6" class="advance-options" style="min-width: 168px;">Tây-Bắc</li>
                                <li vl="7" class="advance-options" style="min-width: 168px;">Tây-Nam</li>
                                <li vl="8" class="advance-options" style="min-width: 168px;">Đông-Nam</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divProject" class="comboboxs advance-select-box adv-search" title=""
                         style="display: none;">
                        <span class="select-text"><input type="text" class="select-text-content" value="--Chọn dự án--"
                                                         placeholder="--Chọn dự án--" style="width: 175px;"></span>
                        <input type="hidden" id="hdCboProject" name="cboListProj" value="0">
                        <div id="divProjectOptions" class="advance-select-options advance-options"
                             hddvalue="hdCboProject" ddlid="divProject" style="">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="0" class="advance-options" style="min-width: 168px;">--Chọn dự án--</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="timkiems">
                    <a id="lblSearch">Tìm nâng cao</a>
                    <input type="image" name="action" class="btnSearch"
                           src="https://file4.batdongsan.com.vn/images/opt/timkiem.jpg">
                    <div style="display: none;">
                        <div id="divCategory" style="">
<span class="select-text hasvalue" style="">
<span class="select-text-content">Nhà đất bán</span>
</span>
                            <input type="hidden" name="cboCategory" id="hdCboCatagory" value="38">
                            <div id="divCatagoryOptions" class="advance-select-options" hddvalue="hdCboCatagory"
                                 ddlid="divCategory" style="">
                                <ul style="min-width: 0px;">
                                    <li vl="0">--Chọn loại nhà đât--</li>
                                    <li vl="38">Nhà đất bán</li>
                                    <li vl="49">Nhà đất cho thuê</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="message" style="">
                    Có <strong>31.309</strong> tin mới trong ngày
                </div>
            </div>
        </div>
        <div style="display: none" id="divReSaler">
            <div class="home-product-search">
                <div id="searchArea">
                    <div id="divBrokerCategory" class="comboboxs advance-select-box" style="">
                        <span class="select-text" style="">
                        <span class="select-text-content" style="width: 75px;">--Chọn loại giao dịch--</span>
                        </span>
                        <input type="hidden" name="cmbCategory" id="hdBrokerCategory" value="0">
                        <div id="divBrokerCategoryOptions" class="advance-select-options advance-options"
                             hddvalue="hdBrokerCategory" ddlid="divBrokerCategory" style="width: 200px;">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="0" class="advance-options" style="min-width: 168px;">--Chọn loại giao dịch--
                                </li>
                                <li vl="38" class="advance-options" style="min-width: 168px;">
                                    Nhà đất bán
                                </li>
                                <li vl="49" class="advance-options" style="min-width: 168px;">
                                    Nhà đất cho thuê
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="divBrokerTypeBDS" class="comboboxs advance-select-box" style="" title="">
                        <span class="select-text" style="">
                        <span class="select-text-content" style="width: 75px;">--Chọn loại nhà đất--</span>
                        </span>
                        <input type="hidden" name="cmbTypeBDS" id="hdBrokerTypeBDS" value="0">
                        <div id="divBrokerTypeBDSOptions" class="advance-select-options advance-options"
                             hddvalue="hdBrokerTypeBDS" ddlid="divBrokerTypeBDS" style="width: 200px;">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li class="advance-options" vl="0" style="min-width: 168px;">--Chọn loại nhà đất--</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divBrokerCity" class="comboboxs advance-select-box" style="">
                        <span class="select-text" style="">
                        <span class="select-text-content" style="width: 75px;">--Chọn Tỉnh/Thành phố--</span>
                        </span>
                        <input type="hidden" name="cmbCity" id="hdBrokerCity" value="CN">
                        <div id="divBrokerCityOptions" class="advance-select-options advance-options"
                             hddvalue="hdBrokerCity" ddlid="divBrokerCity" style="width: 200px;">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="CN" class="advance-options" style="min-width: 168px;">--Chọn Tỉnh/Thành phố--
                                </li>
                                <li vl="SG" class="advance-options" style="min-width: 168px;">Hồ Chí Minh</li>
                                <li vl="HN" class="advance-options" style="min-width: 168px;">Hà Nội</li>
                                <li vl="DDN" class="advance-options" style="min-width: 168px;">Đà Nẵng</li>
                                <li vl="BD" class="advance-options" style="min-width: 168px;">Bình Dương</li>
                                <li vl="DNA" class="advance-options" style="min-width: 168px;">Đồng Nai</li>
                                <li vl="KH" class="advance-options" style="min-width: 168px;">Khánh Hòa</li>
                                <li vl="HP" class="advance-options" style="min-width: 168px;">Hải Phòng</li>
                                <li vl="LA" class="advance-options" style="min-width: 168px;">Long An</li>
                                <li vl="QNA" class="advance-options" style="min-width: 168px;">Quảng Nam</li>
                                <li vl="VT" class="advance-options" style="min-width: 168px;">Bà Rịa Vũng Tàu</li>
                                <li vl="DDL" class="advance-options" style="min-width: 168px;">Đắk Lắk</li>
                                <li vl="CT" class="advance-options" style="min-width: 168px;">Cần Thơ</li>
                                <li vl="BTH" class="advance-options" style="min-width: 168px;">Bình Thuận</li>
                                <li vl="LDD" class="advance-options" style="min-width: 168px;">Lâm Đồng</li>
                                <li vl="TTH" class="advance-options" style="min-width: 168px;">Thừa Thiên Huế</li>
                                <li vl="KG" class="advance-options" style="min-width: 168px;">Kiên Giang</li>
                                <li vl="BN" class="advance-options" style="min-width: 168px;">Bắc Ninh</li>
                                <li vl="QNI" class="advance-options" style="min-width: 168px;">Quảng Ninh</li>
                                <li vl="TH" class="advance-options" style="min-width: 168px;">Thanh Hóa</li>
                                <li vl="NA" class="advance-options" style="min-width: 168px;">Nghệ An</li>
                                <li vl="HD" class="advance-options" style="min-width: 168px;">Hải Dương</li>
                                <li vl="GL" class="advance-options" style="min-width: 168px;">Gia Lai</li>
                                <li vl="BP" class="advance-options" style="min-width: 168px;">Bình Phước</li>
                                <li vl="HY" class="advance-options" style="min-width: 168px;">Hưng Yên</li>
                                <li vl="BDD" class="advance-options" style="min-width: 168px;">Bình Định</li>
                                <li vl="TG" class="advance-options" style="min-width: 168px;">Tiền Giang</li>
                                <li vl="TB" class="advance-options" style="min-width: 168px;">Thái Bình</li>
                                <li vl="BG" class="advance-options" style="min-width: 168px;">Bắc Giang</li>
                                <li vl="HB" class="advance-options" style="min-width: 168px;">Hòa Bình</li>
                                <li vl="AG" class="advance-options" style="min-width: 168px;">An Giang</li>
                                <li vl="VP" class="advance-options" style="min-width: 168px;">Vĩnh Phúc</li>
                                <li vl="TNI" class="advance-options" style="min-width: 168px;">Tây Ninh</li>
                                <li vl="TN" class="advance-options" style="min-width: 168px;">Thái Nguyên</li>
                                <li vl="LCA" class="advance-options" style="min-width: 168px;">Lào Cai</li>
                                <li vl="NDD" class="advance-options" style="min-width: 168px;">Nam Định</li>
                                <li vl="QNG" class="advance-options" style="min-width: 168px;">Quảng Ngãi</li>
                                <li vl="BTR" class="advance-options" style="min-width: 168px;">Bến Tre</li>
                                <li vl="DNO" class="advance-options" style="min-width: 168px;">Đắk Nông</li>
                                <li vl="CM" class="advance-options" style="min-width: 168px;">Cà Mau</li>
                                <li vl="VL" class="advance-options" style="min-width: 168px;">Vĩnh Long</li>
                                <li vl="NB" class="advance-options" style="min-width: 168px;">Ninh Bình</li>
                                <li vl="PT" class="advance-options" style="min-width: 168px;">Phú Thọ</li>
                                <li vl="NT" class="advance-options" style="min-width: 168px;">Ninh Thuận</li>
                                <li vl="PY" class="advance-options" style="min-width: 168px;">Phú Yên</li>
                                <li vl="HNA" class="advance-options" style="min-width: 168px;">Hà Nam</li>
                                <li vl="HT" class="advance-options" style="min-width: 168px;">Hà Tĩnh</li>
                                <li vl="DDT" class="advance-options" style="min-width: 168px;">Đồng Tháp</li>
                                <li vl="ST" class="advance-options" style="min-width: 168px;">Sóc Trăng</li>
                                <li vl="KT" class="advance-options" style="min-width: 168px;">Kon Tum</li>
                                <li vl="QB" class="advance-options" style="min-width: 168px;">Quảng Bình</li>
                                <li vl="QT" class="advance-options" style="min-width: 168px;">Quảng Trị</li>
                                <li vl="TV" class="advance-options" style="min-width: 168px;">Trà Vinh</li>
                                <li vl="HGI" class="advance-options" style="min-width: 168px;">Hậu Giang</li>
                                <li vl="SL" class="advance-options" style="min-width: 168px;">Sơn La</li>
                                <li vl="BL" class="advance-options" style="min-width: 168px;">Bạc Liêu</li>
                                <li vl="YB" class="advance-options" style="min-width: 168px;">Yên Bái</li>
                                <li vl="TQ" class="advance-options" style="min-width: 168px;">Tuyên Quang</li>
                                <li vl="DDB" class="advance-options" style="min-width: 168px;">Điện Biên</li>
                                <li vl="LCH" class="advance-options" style="min-width: 168px;">Lai Châu</li>
                                <li vl="LS" class="advance-options" style="min-width: 168px;">Lạng Sơn</li>
                                <li vl="HG" class="advance-options" style="min-width: 168px;">Hà Giang</li>
                                <li vl="BK" class="advance-options" style="min-width: 168px;">Bắc Kạn</li>
                                <li vl="CB" class="advance-options" style="min-width: 168px;">Cao Bằng</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divBrokerDistrict" class="comboboxs advance-select-box" style="" title="">
                    <span class="select-text" style="">
                    <span class="select-text-content" style="width: 75px;">--Chọn Quận/Huyện--</span>
                    </span>
                        <input type="hidden" name="cmbDistrict" id="hdBrokerDistrict" value="0">
                        <div id="divBrokerDistrictOptions" class="advance-select-options advance-options"
                             hddvalue="hdBrokerDistrict" ddlid="divBrokerDistrict" style="width: 200px;">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="0" class="advance-options" style="min-width: 168px;">--Chọn Quận/Huyện--</li>
                            </ul>
                        </div>
                    </div>
                    <div id="divBrokerProject" class="comboboxs advance-select-box" style="" title="">
                        <span class="select-text" style="">
                        <span class="select-text-content" style="width: 75px;">--Chọn dự án--</span>
                        </span>
                        <input type="hidden" name="cmbProject" id="hdBrokerProject" value="0">
                        <div id="divBrokerProjectOptions" class="advance-select-options advance-options"
                             hddvalue="hdBrokerProject" ddlid="divBrokerProject" style="width: 200px;">
                            <ul class="advance-options" style="min-width: 200px;">
                                <li vl="0" class="advance-options" style="min-width: 168px;">--Chọn dự án--</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="timkiems">
                    <input type="image" name="action" class="btnSearch"
                           src="https://file4.batdongsan.com.vn/images/opt/timkiem.jpg">
                </div>
            </div>
        </div>
    </div>
</div>