@extends('front.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="    margin-bottom: 10px;">
                <div class="wrapper">
                    <!-- News Slider -->
                    <div class="ticker marg-botm">
                        <div class="ticker-wrap">
                            <!-- News Slider Title -->
                            <div class="ticker-head up-case backg-colr col-md-2">Breaking News <i class="fa fa-angle-double-right" aria-hidden="true"></i></div>
                            <div class="tickers col-md-10">
                                <div id="top-news-slider" class="owl-carousel ">
                                    @foreach($tinTuc as $published)
                                        <div class="item">

                                            <a href="{{ url('/single-news/'.$published->id) }}"> <img src=" home/tintuc/{{ $published->thumbnail }}" alt="{{ $published->title }}"> <span>{{$published->title}} </span></a>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End News Slider -->
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                @include('front.search')
                @include('front.tinnoibat1')
                @include('front.xaydung')
                @include('front.tabContent')
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                @include('front.tinxemnhieunhat')
                @include('front.tinsidebar')
            </div>
        </div>

    </div>
@endsection