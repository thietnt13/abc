

<div class='col-sm-3 form-search'>
    <div class="row search">

        <div class="panel-heading" style="margin-bottom: 5px">
            <h3 class="panel-title">TÌM KIẾM</h3>
        </div>
        <form method ="post" id="side_bar" action="{{Asset('loai-tin/search')}}" class="form-horizontal ">
            <div class="col-sm-12 choose-search">
                <div class="form-group" >
                    <div style="padding-left:0px"  class="col-sm-12" >
                        <select class="form-control" id="category" name="category" >
                            <option style="color:gray" value="">Chọn thể loại</option>
                            @foreach($theLoai as $the_loai)
                                <option value="{{$the_loai->id}}">{{$the_loai->Ten}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
               {{-- <div class="form-group">
                    <div class="col-sm-12" style="padding-left:0px" >
                        <select class="form-control" id="city" name="city">
                            <option style="color:gray" value="">Chọn Tỉnh/TP</option>
                            @foreach($cityinfo as $city)
                            <option value="1">Hà Nội</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group" id="display-sibar-district">
                    <div class="col-sm-12" style="padding-left:0px" >
                        <select class="form-control" id="district" name="district" >
                            <option style="color:gray" value="">Chọn Quận/Huyện</option>
                            @foreach($districts as $district)
                            <option value="1">Chương mỹ</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group" id="display-sibar-street">
                    <div class="col-sm-12" style="padding-left:0px" >
                        <select class="form-control" id="street" name="street" >
                            <option style="color:gray" value="">Chọn Đường</option>
                            @foreach($streetinfo as $street)
                            <option value="1">Thượng Vực}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
--}}
            </div>
            <div style="    margin-bottom: 10px;">
                <input type="submit" class="btn btn-info hvr-wobble-to-bottom-right" value="Tìm Kiếm">
                <button id="btnclear" name="btnclear" class="btn btn-default hvr-float-shadow">Làm trắng</button>
            </div>
        </form>
    </div>

</div>

<!--End panel-->

