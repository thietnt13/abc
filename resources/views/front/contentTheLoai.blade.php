<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="row">
        <div class="tab-home">
            <ul class="nav nav-tabs">
                <li class="title-bg" style="background-color: #055699;color: white;padding: 1px 10px; font-size: 14px;font-weight: bold;">{{$the_loai_by_id[0]->Ten}}</li>
                <?php
                $tin1=$loai_tin_by_theloai->shift();
                ?>
                @if(count($tin_tuc_theloai->where('idLoaiTin',$tin1->id))>0)
                    <li class="active"><a data-toggle="tab" href="#tab{{$tin1->id}}">{{$tin1->Ten}}</a></li>
                @endif
                @foreach($loai_tin_by_theloai->all() as $loai_tin)
                    @if(count($tin_tuc_theloai->where('idLoaiTin',$loai_tin->id))>0)
                        <li><a data-toggle="tab" href="#tab{{$loai_tin->id}}">{{$loai_tin->Ten}}</a></li>
                    @endif
                @endforeach
            </ul>
            <div class="tab-content">
                @if(count($tin_tuc_theloai->where('idLoaiTin',$tin1->id))>0)
                <div id="tab1" class="tab-pane fade in active">
                    <?php
                    $tintabdau=$tin_tuc_theloai->where('idLoaiTin',$tin1->id)->sortByDesc('created_at')->take(5);
                    $tintuctop=$tintabdau->shift();
                    ?>
                    <div class="tab-top-content">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 paddimg-right-none">
                                <a href="tintuc/{{$tintuctop->id}}"><img src="home/tintuc/{{$tintuctop->Hinh}}" alt="sidebar image"></a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 last-col">
                                <span class="date"><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> {{$tintuctop ->SoLuotXem}}</a></span>
                                <h3><a href="tintuc/{{$tintuctop->id}}">{{$tintuctop->TieuDe}}</a></h3>
                                <?php
                                $tom_tat = substr($tintuctop->TomTat, 0, 150);
                                $pos = strrpos($tom_tat, " ");
                                $tom_tat = substr($tom_tat, 0, $pos);

                                ?>
                                <p>{{$tom_tat}}....</p>
                                <a href="tintuc/{{$tintuctop->id}}" class="read-more hvr-bounce-to-right">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-bottom-content">
                        <div class="row">
                            @foreach($tintabdau->all() as $tin)
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tab-area">
                                    <div class="col-sm-12 col-xs-3 img-tab">
                                        <a href="tintuc/{{$tin->id}}"><img src="home/tintuc/{{$tin->Hinh}}" alt="News image"></a>
                                    </div>
                                    <div class="col-sm-12 col-xs-9 img-content">
                                        <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"> </i>{{$tin->created_at}}</span>
                                        <?php
                                        $title = substr($tin->TieuDe, 0, 60);
                                        $pos = strrpos($title, " ");
                                        $title = substr($title, 0, $pos);

                                        ?>
                                        <p><a href="tintuc/{{$tin->id}}">{{$title}}...</a></p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                @endif
                @foreach($loai_tin_by_theloai->all() as $loai_tin)
                    @if(count($tin_tuc_theloai->where('idLoaiTin',$loai_tin->id))>0)
                    <div id="tab{{$loai_tin->id}}" class="tab-pane fade">
                        <?php
                        $tintab=$tin_tuc_theloai->where('idLoaiTin',$loai_tin->id)->sortByDesc('created_at')->take(5);
                        $tintop=$tintab->shift();
                        ?>
                        <div class="tab-top-content">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 paddimg-right-none">
                                    <a href="tintuc/{{$tintop->id}}"><img src="home/tintuc/{{$tintop->Hinh}}" alt="sidebar image"></a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 last-col">
                                    <span class="date"><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> {{$tintop->SoLuotXem}}</a></span>
                                    <h3><a href="tintuc/{{$tintop->id}}">{{$tintop->TieuDe}}</a></h3>
                                    <?php
                                    $tom_tat = substr($tintop->TomTat, 0, 150);
                                    $pos = strrpos($tom_tat, " ");
                                    $tom_tat = substr($tom_tat, 0, $pos);
                                    ?>
                                    <p>{{$tom_tat}}....</p>
                                    <a href="tintuc/{{$tintop->id}}" class="read-more hvr-bounce-to-right">Xem thêm</a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-bottom-content">
                            <div class="row">
                                @foreach($tintab->all() as $tin)
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tab-area">
                                        <div class="col-sm-12 col-xs-3 img-tab">
                                            <a href="tintuc/{{$tin->id}}"><img src="home/tintuc/{{$tin->Hinh}}" alt="News image"></a>
                                        </div>
                                        <div class="col-sm-12 col-xs-9 img-content">
                                            <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"> </i>{{$tin->created_at}}</span>
                                            <?php
                                            $title = substr($tin->TieuDe, 0, 60);
                                            $pos = strrpos($title, " ");
                                            $title = substr($title, 0, $pos);

                                            ?>
                                            <p><a href="tintuc/{{$tin->id}}">{{$title}}...</a></p>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
            <!-- Trending news  here-->

            <!--Start what’s hot now -->
            <div class="hot-news" style="margin-top: 5px">
                <?php
                $tin=$tinTuc->take(4);
                $tin1=$tin->shift();
                ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="view-area">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h3 class="title-bg">Tin khác</h3>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <a href="#">Nhiều hơn <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="featured">
                            <div class="blog-img">
                                <a href="tintuc/{{$tin1->id}}"><img style="height: 300px" src="home/tintuc/{{$tin1->Hinh}}" alt="" title="News image"></a>
                            </div>
                            <div class="blog-content">
                                <a href="tintuc/{{$tin1->id}}" class="btn-small">Sports</a><span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> November 28, 2017</span>
                                <h4><a href="tintuc/{{$tin1->id}}">{{$tin1->TieuDe}}</a></h4>
                            </div>
                        </div>
                        <ul class="news-post news-feature-mb">

                            @foreach($tin as $tin_tuc)
                                <li>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-4">
                                            <a href="tintuc/{{$tin_tuc->id}}"><img src="home/tintuc/{{$tin_tuc->Hinh}}" alt="News image"></a>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-8 content">
                                            <h4><a href="tintuc/{{$tin_tuc->id}}">{{$tin_tuc->TieuDe}}</a></h4>
                                            <span class="author"><a href="tintuc/{{$tin_tuc->id}}"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{$tin_tuc->created_at}}</span>
                                            <span class="comment"><a href="tintuc/{{$tin_tuc->id}}"><i class="fa fa-comment-o" aria-hidden="true"></i> 50</a></span>
                                            <p>{{$tin_tuc->TomTat}}</p>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End what’s hot now -->
        </div>

    </div>
</div>