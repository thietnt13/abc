@extends('front.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                @include('front.contentTinTuc')
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                @include('front.tinxemnhieunhat')
                @include('front.tinsidebar')
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 col-lg-9 col-xs-9">

            </div>
        </div>
    </div>
@endsection