<div class='col-sm-6 kinhnghiem'>
    <div class='col-sm-12 form-kinhnghiem hvr-glow'>
        <div class='row category-kinhnghiem'>
            <span class='span-kinhnghiem'>KINH NGHIỆM</span>
        </div>
        <div class='row content-kinhnghiem'>
            @if(!empty($tinTuc))
                <?php $i = 0;?>
                @foreach($tinTuc as $tin_tuc)
                    <?php $i++;?>
                    @if($i==1)
                        <div class="col-sm-3 image-kinhnghiem">
                            <a href="" id="">
                                <img src="home/tintuc/{{$tin_tuc->Hinh}}" class="img-responsive img-category" alt=""/>
                            </a>
                        </div>
                        <div class="col-sm-9 detail-kinhnghiem">

                            <div class="row title-detail-kinhnghiem">
                                <a href="" src="home/tintuc/{{$tin_tuc->Hinh}}"><span
                                            class='span-bold-blue'>{{$tin_tuc->TieuDe}}</span></a>
                            </div>
                            <div class="row content-detail-kinhnghiem">
                                {{$tin_tuc->TieuDe}}
                            </div>
                            @endif
                            @endforeach
                            @endif

                            <div class="row other-kinhnghiem">
                                <ul class="list-unstyled ">
                                    @if(!empty($tinTuc))
                                        <?php $i = 0;?>
                                        @foreach($tinTuc as $tin_tuc)
                                            <?php $i++;?>
                                            @if($i!=1)
                                                <li class="link-other-kinhnghiem">
                                                    <a href="" id="btn-news-categoty-top" class="">
                                                        <span class="viewed-name">{{$tin_tuc->Ten}}</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
        </div>
    </div>
</div>
<!--End panel-->

