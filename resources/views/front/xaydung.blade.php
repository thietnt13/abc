<div class="col-md-12">
    <div class="trending-news separator-large">
        <div class="row" style="border: 1px solid #ccc;">
            <div class="view-area">
                <div class="col-sm-12 heading" style="    padding-left: 0px;">
                    <h3 class="title-bg"><span>Xây dựng</span></h3>
                </div>
            </div>
            <div class="line_gr"></div>
            <?php
            $tinXayDung=$tinTuc->where('category_id',3)->sortByDesc('created_at')->take(5);
            $tin1=$tinXayDung->shift();
            ?>
            <div class="row content">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="list-col">
                        <a href="tintuc/{{$tin1['id']}}"> <img src="home/tintuc/{{$tin1['thumbnail']}}" alt="" title="Trending image"></a>
                        <div class="dsc">
                            <span class="date"> <i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{$tin1['created_at']}}/span> <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> {{$tin1['SoLuotXem']}}</a></span></span>
                            <h3><a href="tintuc/{{$tin1['id']}}">{{$tin1['title']}} </a></h3>
                            <p>{{$tin1['title']}}.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <ul class="news-post">
                        @foreach($tinXayDung->all() as $tin_tuc)
                            <li>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content">
                                        <div class="item-post">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-3 paddimg-right-none">
                                                    <a href="tintuc/{{$tin_tuc['id']}}"> <img src="home/tintuc/{{$tin_tuc['thumbnail']}}" alt="" title="Trending image"></a>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
                                                    <h4><a href="tintuc/{{$tin_tuc['id']}}">{{$tin_tuc['title']}}</a></h4>
                                                    <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> {{$tin_tuc['created_at']}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>
    </div>

</div>