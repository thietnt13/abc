<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="row" >
        <div class="tab-home" style="border:1px solid #ccc">
            <ul class="nav nav-tabs">
                <li class="title-bg" style="background-color: #055699;color: white;padding: 1px 10px; font-size: 14px;font-weight: bold;">Tin tức</li>
                <li class="active"><a data-toggle="tab" href="#tab1">Tin địa phương</a></li>
                <li><a data-toggle="tab" href="#tab2">Tin hoạt động</a></li>
                <li><a data-toggle="tab" href="#tab3">Tin thị trường</a></li>
                <li><a data-toggle="tab" href="#tab4">Tin tổng hợp</a></li>
                <li><a data-toggle="tab" href="#tab5">Tin trong ngành</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab1" class="tab-pane fade in active">
                    <?php
                    $tintab1=$tinTuc->where('idLoaiTin',10)->sortByDesc('created_at')->take(5);
                    $tin1=$tintab1->shift();
                    ?>
                    <div class="tab-top-content">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 paddimg-right-none">
                                <a href="tintuc/{{$tin1['id']}}"><img src="home/tintuc/{{$tin1['Hinh']}}" alt="sidebar image"></a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 last-col">
                                <span class="date"><a href="tintuc/{{$tin1['id']}}"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> {{$tin1['SoLuotXem']}}</a></span>
                                <h3><a href="tintuc/{{$tin1['id']}}">{{$tin1['TieuDe']}}</a></h3>
                                <?php
                                $tom_tat = substr($tin1['TomTat'], 0, 150);
                                $pos = strrpos($tom_tat, " ");
                                $tom_tat = substr($tom_tat, 0, $pos);

                                ?>
                                <p>{{$tom_tat}}....</p>
                                <a href="tintuc/{{$tin1['id']}}" class="read-more hvr-bounce-to-right">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-bottom-content">
                        <div class="row">
                            @foreach($tintab1->all() as $tin)
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tab-area">
                                    <div class="col-sm-12 col-xs-3 img-tab">
                                        <a href="tintuc/{{$tin['id']}}"><img style="    height: 120px;" src="home/tintuc/{{$tin['Hinh']}}" alt="News image"></a>
                                    </div>
                                    <div class="col-sm-12 col-xs-9 img-content">
                                        <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"> </i>{{$tin['created_at']}}</span>
                                        <?php
                                        $title = substr($tin['TieuDe'], 0, 60);
                                        $pos = strrpos($title, " ");
                                        $title = substr($title, 0, $pos);

                                        ?>
                                        <p><a href="tintuc/{{$tin['id']}}">{{$title}}...</a></p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div id="tab2" class="tab-pane fade">
                    <?php
                    $tintab1=$tinTuc->where('idLoaiTin',8)->sortByDesc('created_at')->take(5);
                    $tin1=$tintab1->shift();
                    ?>
                    <div class="tab-top-content">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 paddimg-right-none">
                                <a href="tintuc/{{$tin1['id']}}"><img src="home/tintuc/{{$tin1['Hinh']}}" alt="sidebar image"></a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 last-col">
                                <span class="date"><a href="tintuc/{{$tin1['id']}}"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> {{$tin1['SoLuotXem']}}</a></span>
                                <h3><a href="tintuc/{{$tin1['id']}}">{{$tin1['TieuDe']}}</a></h3>
                                <?php
                                $tom_tat = substr($tin1['TomTat'], 0, 150);
                                $pos = strrpos($tom_tat, " ");
                                $tom_tat = substr($tom_tat, 0, $pos);

                                ?>
                                <p>{{$tom_tat}}....</p>
                                <a href="tintuc/{{$tin1['id']}}" class="read-more hvr-bounce-to-right">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-bottom-content">
                        <div class="row">
                            @foreach($tintab1->all() as $tin)
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tab-area">
                                    <div class="col-sm-12 col-xs-3 img-tab">
                                        <a href="tintuc/{{$tin['id']}}"><img style="height: 120px" src="home/tintuc/{{$tin['Hinh']}}" alt="News image"></a>
                                    </div>
                                    <div class="col-sm-12 col-xs-9 img-content">
                                        <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"> </i>{{$tin['created_at']}}</span>
                                        <?php
                                        $title = substr($tin['TieuDe'], 0, 60);
                                        $pos = strrpos($title, " ");
                                        $title = substr($title, 0, $pos);

                                        ?>
                                        <p><a href="tintuc/{{$tin['id']}}">{{$title}}...</a></p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div id="tab3" class="tab-pane fade">
                    <?php
                    $tintab1=$tinTuc->where('idLoaiTin',3)->sortByDesc('created_at')->take(5);
                    $tin1=$tintab1->shift();
                    ?>
                    <div class="tab-top-content">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 paddimg-right-none">
                                <a href="tintuc/{{$tin1['id']}}"><img src="home/tintuc/{{$tin1['Hinh']}}" alt="sidebar image"></a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 last-col">
                                <span class="date"><a href="tintuc/{{$tin1['id']}}"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> {{$tin1['SoLuotXem']}}</a></span>
                                <h3><a href="tintuc/{{$tin1['id']}}">{{$tin1['TieuDe']}}</a></h3>
                                <?php
                                $tom_tat = substr($tin1['TomTat'], 0, 150);
                                $pos = strrpos($tom_tat, " ");
                                $tom_tat = substr($tom_tat, 0, $pos);

                                ?>
                                <p>{{$tom_tat}}....</p>
                                <a href="tintuc/{{$tin1['id']}}" class="read-more hvr-bounce-to-right">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-bottom-content">
                        <div class="row">
                            @foreach($tintab1->all() as $tin)
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tab-area">
                                    <div class="col-sm-12 col-xs-3 img-tab">
                                        <a href="tintuc/{{$tin['id']}}"><img style="height: 120px;" src="home/tintuc/{{$tin['Hinh']}}" alt="News image"></a>
                                    </div>
                                    <div class="col-sm-12 col-xs-9 img-content">
                                        <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"> </i>{{$tin['created_at']}}</span>
                                        <?php
                                        $title = substr($tin['TieuDe'], 0, 60);
                                        $pos = strrpos($title, " ");
                                        $title = substr($title, 0, $pos);

                                        ?>
                                        <p><a href="tintuc/{{$tin['id']}}">{{$title}}...</a></p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div id="tab4" class="tab-pane fade">
                    <?php
                    $tintab1=$tinTuc->where('idLoaiTin',9)->sortByDesc('created_at')->take(5);
                    $tin1=$tintab1->shift();
                    ?>
                    <div class="tab-top-content">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 paddimg-right-none">
                                <a href="tintuc/{{$tin1['id']}}"><img src="home/tintuc/{{$tin1['Hinh']}}" alt="sidebar image"></a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 last-col">
                                <span class="date"><a href="tintuc/{{$tin1['id']}}"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> {{$tin1['SoLuotXem']}}</a></span>
                                <h3><a href="tintuc/{{$tin1['id']}}">{{$tin1['TieuDe']}}</a></h3>
                                <?php
                                $tom_tat = substr($tin1['TomTat'], 0, 150);
                                $pos = strrpos($tom_tat, " ");
                                $tom_tat = substr($tom_tat, 0, $pos);

                                ?>
                                <p>{{$tom_tat}}....</p>
                                <a href="tintuc/{{$tin1['id']}}" class="read-more hvr-bounce-to-right">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-bottom-content">
                        <div class="row">
                            @foreach($tintab1->all() as $tin)
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tab-area">
                                    <div class="col-sm-12 col-xs-3 img-tab">
                                        <a href="tintuc/{{$tin['id']}}"><img style="height: 120px;" src="home/tintuc/{{$tin['Hinh']}}" alt="News image"></a>
                                    </div>
                                    <div class="col-sm-12 col-xs-9 img-content">
                                        <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"> </i>{{$tin['created_at']}}</span>
                                        <?php
                                        $title = substr($tin['TieuDe'], 0, 60);
                                        $pos = strrpos($title, " ");
                                        $title = substr($title, 0, $pos);

                                        ?>
                                        <p><a href="tintuc/{{$tin['id']}}">{{$title}}...</a></p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div id="tab5" class="tab-pane fade">
                    <?php
                    $tintab1=$tinTuc->where('idLoaiTin',11)->sortByDesc('created_at')->take(5);
                    $tin1=$tintab1->shift();
                    ?>
                    <div class="tab-top-content">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 paddimg-right-none">
                                <a href="tintuc/{{$tin1['id']}}"><img src="home/tintuc/{{$tin1['Hinh']}}" alt="sidebar image"></a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 last-col">
                                <span class="date"><a href="tintuc/{{$tin1['id']}}"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> {{$tin1['SoLuotXem']}}</a></span>
                                <h3><a href="tintuc/{{$tin1['id']}}">{{$tin1['TieuDe']}}</a></h3>
                                <?php
                                $tom_tat = substr($tin1['TomTat'], 0, 150);
                                $pos = strrpos($tom_tat, " ");
                                $tom_tat = substr($tom_tat, 0, $pos);

                                ?>
                                <p>{{$tom_tat}}....</p>
                                <a href="tintuc/{{$tin1['id']}}" class="read-more hvr-bounce-to-right">Xem thêm</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-bottom-content">
                        <div class="row">
                            @foreach($tintab1->all() as $tin)
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tab-area">
                                    <div class="col-sm-12 col-xs-3 img-tab">
                                        <a href="tintuc/{{$tin['id']}}"><img style="height: 120px;" src="home/tintuc/{{$tin['Hinh']}}" alt="News image"></a>
                                    </div>
                                    <div class="col-sm-12 col-xs-9 img-content">
                                        <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"> </i>{{$tin['created_at']}}</span>
                                        <?php
                                        $title = substr($tin['TieuDe'], 0, 60);
                                        $pos = strrpos($title, " ");
                                        $title = substr($title, 0, $pos);

                                        ?>
                                        <p><a href="tintuc/{{$tin['id']}}">{{$title}}...</a></p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>