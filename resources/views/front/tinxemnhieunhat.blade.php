<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
    <div class="row tinpanel recent-post-area">
        <div class="panel-heading">
            <h3 class="panel-title">TIN XEM NHIỀU NHẤT</h3>
        </div>
        <?php
            $tinXemNhieu=$tinTuc->sortByDesc('SoLuotXem')->take(2)
        ?>
        @foreach($tinXemNhieu as $tin_tuc)
        <div class="card">
            <img class="card-img-top" href="tintuc/{{$tin_tuc['id']}}" src="home/tintuc/{{$tin_tuc->Hinh}}" alt="Card image cap">
            <div class="card-body">
                <a href="tintuc/{{$tin_tuc['id']}}"><h5 class="card-title">{{$tin_tuc->TieuDe}}</h5></a>
            </div>
        </div>
            @endforeach
    </div>
</div>
