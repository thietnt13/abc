<!DOCTYPE html>
<html lang="en">
<head>
    <title>Do An 3</title>
    <meta charset="utf-8">
    <base href="{{asset('')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/front.css">
    <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('/front') }}/css/bootstrap.min.css">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="{{ asset('/front') }}/css/font-awesome.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset('/front') }}/css/animate.css">
    <!-- hover-min css -->
    <link rel="stylesheet" href="{{ asset('/front') }}/css/hover-min.css">
    <!-- magnific-popup css -->
    <link rel="stylesheet" href="{{ asset('/front') }}/css/magnific-popup.css">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="{{ asset('/front') }}/css/meanmenu.min.css">
    <!-- owl.carousel css -->
    <link rel="stylesheet" href="{{ asset('/front') }}/css/owl.carousel.css">
    <!-- lightbox css -->
    <link href="{{ asset('/front') }}/css/lightbox.min.css" rel="stylesheet">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="{{ asset('/front') }}/inc/custom-slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/front') }}/inc/custom-slider/css/preview.css" type="text/css" media="screen" />
    <!-- style css -->
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset('/front') }}/css/responsive.css">
    <link rel="stylesheet" href="front/css/style.css">

    <!-- modernizr js -->
    <script src="{{ asset('/front') }}/js/modernizr-2.8.3.min.js"></script>
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
<div class="container">
    <div class="header container">
        <div class="header-top">
            <div class="header-top-left">
                <div style = "padding-top: 5px">
                    <h1>
                        <img src="http://file4.batdongsan.com.vn/images/Logo/BDS-LogoNew.jpg" alt="Kênh thông tin mua bán, cho thuê nhà đất số 1" style="padding-top: 2px;">
                    </h1>
                </div>
            </div>
            <div class="header-top-right">
                <div id=TopBanner">

                </div>
            </div>
        </div>
    </div>

    @include('front.header')
    @yield('content')
    @include('front.footer')

</div>
<script src="{{ asset('/front') }}/js/jquery.min.js"></script>
<!-- jquery latest version -->
<script src="{{ asset('/front') }}/js/jquery.min.js"></script>
<!-- jquery-ui js -->
<script src="{{ asset('/front') }}/js/jquery-ui.min.js"></script>
<!-- bootstrap js -->
<script src="{{ asset('/front') }}/js/bootstrap.min.js"></script>
<!-- meanmenu js -->
<script src="{{ asset('/front') }}/js/jquery.meanmenu.js"></script>
<!-- wow js -->
<script src="{{ asset('/front') }}/js/wow.min.js"></script>
<!-- owl.carousel js -->
<script src="{{ asset('/front') }}/js/owl.carousel.min.js"></script>
<!-- magnific-popup js -->
<script src="{{ asset('/front') }}/js/jquery.magnific-popup.js"></script>

<!-- jquery.counterup js -->
<script src="{{ asset('/front') }}/js/jquery.counterup.min.js"></script>
<script src="{{ asset('/front') }}/js/waypoints.min.js"></script>
<!-- jquery light box -->
<script src="{{ asset('/front') }}/js/lightbox.min.js"></script>
<!-- Nivo slider js -->
<script src="{{ asset('/front') }}/inc/custom-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="{{ asset('/front') }}/inc/custom-slider/home.js" type="text/javascript"></script>
<!-- main js -->
<script src="{{ asset('/front') }}/js/main.js"></script>

@yield('script')
</body>
</html>
