<div class="hot-news">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="view-area">
                <div class="row">
                    <div class="col-sm-8">
                        <h3 class="title-bg">{{$the_loai_by_id->Ten}}</h3>
                    </div>
                </div>
            </div>
            <?php
            $tintuc=$tinTuc->where('idLoaiTin',$loaitin_by_id->id)->sortByDesc('created_at')->take(5);
            $tintuctop=$tintuc->shift();
            ?>
            <div class="featured">
                <div class="blog-img" style="width: 300px;">
                    <a href="tintuc/{{$tintuctop->id}}" ><img src="home/tintuc/{{$tintuctop->Hinh}}" alt="" title="News image"></a>
                </div>
                <div class="blog-content">
                    <a href="tintuc/{{$tintuctop->id}}" class="cat-link">Sports</a><span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{$tintuctop->created_at}}</span>
                    <h4><a href="tintuc/{{$tintuctop->id}}">{{$tintuctop->TieuDe}}</a></h4>
                </div>
            </div>
            <ul class="news-post news-feature-mb">
                @foreach($tintuc as $tin)
                    <li>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-4">
                                <a href="tintuc/{{$tin->id}}"><img src="home/tintuc/{{$tin->Hinh}}"></a>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-8 content">
                                <h4><a href="tintuc/{{$tin->id}}">{{$tin->TieuDe}}</a></h4>
                                <span class="author"><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Thiet</a></span> <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{$tin->created_at}}</span>
                                <span class="comment"><a href="#"><i class="fa fa-comment-o" aria-hidden="true"></i> 50</a></span>
                                <p>{{$tin->TomTat}}</p>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>