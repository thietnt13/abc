<div class="col-sm-9 col-md-9 tab-home">
    <div class="tab-content" style="border: 1px solid #ccc">
        <div id="tab1" class="tab-pane fade in active">
            <?php
                $tinNoiBat=$tinTuc->where('del_flg',0)->sortByDesc('created_at')->take(5);
                $tin1=$tinNoiBat->shift();
            ?>
            <div class="tab-top-content">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 paddimg-right-none">
                        <a href="tintuc/{{$tin1->id}}"><img style="height: 300px;" src="home/tintuc/{{$tin1['thumbnail']}}" alt="sidebar image"></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 last-col">
                        <h3><a href="tintuc/{{$tin1['id']}}">{{$tin1['title']}}</a></h3>
                        <?php
                        $tom_tat = substr($tin1['title'], 0, 150);
                        $pos = strrpos($tom_tat, " ");
                        $tom_tat = substr($tom_tat, 0, $pos);

                        ?>
                        <p>{{$tom_tat}}....</p>
                        <a href="tintuc/{{$tin1['id']}}" class="read-more hvr-bounce-to-right">Đọc</a>
                    </div>
                </div>
            </div>
            <div class="tab-bottom-content">
                <div class="row">
                    @foreach($tinNoiBat->all() as $tin_tuc)
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 tab-area">
                        <div class="col-sm-12 col-xs-3 img-tab">
                            <a href="tintuc/{{$tin_tuc['id']}}"><img style="height: 100px;" src="home/tintuc/{{$tin_tuc['thumbnail']}}" alt="News image"></a>
                        </div>
                        <div class="col-sm-12 col-xs-9 img-content">
                            <span class="date"><i class="fa fa-calendar-check-o" aria-hidden="true"> </i>{{$tin_tuc['created_at']}}</span>
                            <?php
                            $title = substr($tin_tuc['title'], 0, 60);
                            $pos = strrpos($title, " ");
                            $title = substr($title, 0, $pos);

                            ?>
                            <p><a href="tintuc/{{$tin_tuc['id']}}">{{$title}}...</a></p>
                        </div>
                    </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>